/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>

#include <string>
#include <boost/variant.hpp>

#include "frontend/tcp/CommandParser.h"
#include "rts/SomfyFrame.h"

namespace
{
	class CommandTester: private CommandListener
	{
	private:
		struct ExpectList
		{
		};

		struct ExpectCreate
		{
			std::string name;
			uint32_t address;
			uint16_t nextRollingCode;
		};

		struct ExpectSet
		{
			std::string oldName;
			std::string newName;
			std::optional<uint32_t> address;
			std::optional<uint16_t> nextRollingCode;
			std::optional<uint8_t> key;
			std::optional<int> priority;
		};

		struct ExpectRemove
		{
			std::string name;
		};

		struct ExpectBroadcast
		{
			std::string name;
			rts::SomfyFrame::Action action;
		};

		struct ExpectHelp
		{
		};

	public:
		CommandTester(std::string && u8Input):
			m_u8Input(std::move(u8Input))
		{
		}

		CommandTester & list()
		{
			m_expected.emplace_back(ExpectList{});
			return *this;
		}

		CommandTester & create(const std::string & name, uint32_t address, uint16_t nextRollingCode)
		{
			m_expected.emplace_back(ExpectCreate{name, address, nextRollingCode});
			return *this;
		}

		CommandTester & set(const std::string & oldName, const std::string & newName,
			const std::optional<uint32_t> & address,
			const std::optional<uint16_t> & nextRollingCode,
			const std::optional<uint8_t> & key,
			const std::optional<int> & priority)
		{
			m_expected.emplace_back(ExpectSet{oldName, newName, address, nextRollingCode, key, priority});
			return *this;
		}

		CommandTester & remove(std::string && name)
		{
			m_expected.emplace_back(ExpectRemove{std::move(name)});
			return *this;
		}

		CommandTester & broadcast(std::string && name, rts::SomfyFrame::Action action)
		{
			m_expected.emplace_back(ExpectBroadcast{std::move(name), action});
			return *this;
		}

		CommandTester & help()
		{
			m_expected.emplace_back(ExpectHelp{});
			return *this;
		}

		void run()
		{
			m_ignoreCallbacks = false;
			m_current = m_expected.begin();
			runImpl();

			if (m_current != m_expected.end())
				BOOST_FAIL("not all expected commands were received");
		}

		void runOnly()
		{
			m_ignoreCallbacks = true;
			runImpl();
		}

	private:
		void runImpl()
		{
			CommandParser c(*this);
			c.accept(m_u8Input.begin(), m_u8Input.end());
			c.end();
		}

		template<typename ExpectT>
		ExpectT * getNext()
		{
			if (m_ignoreCallbacks)
				return nullptr;

			ExpectT * e = nullptr;

			if (m_current != m_expected.end())
			{
				e = boost::get<ExpectT>(&*m_current++);
				if (!e)
					BOOST_FAIL("received unexpected command");
			}
			else
				BOOST_FAIL("received munexpected command");

			return e;
		}

		virtual void onList() override
		{
			if (m_ignoreCallbacks)
				return;

			getNext<ExpectList>();
		}

		virtual void onCreate(std::string && name, uint32_t address, uint16_t nextRollingCode) override
		{
			ExpectCreate * c = getNext<ExpectCreate>();
			if (c)
			{
				BOOST_TEST(name == c->name);
				BOOST_TEST(address == c->address);
				BOOST_TEST(nextRollingCode == c->nextRollingCode);
			}
		}

		virtual void onSet(std::string && oldName, std::string && newName, const std::optional<uint32_t> & address,
			const std::optional<uint16_t> & nextRollingCode, const std::optional<uint8_t> & key,
			const std::optional<int> & priority) override
		{
			ExpectSet * c = getNext<ExpectSet>();
			if (c)
			{
				BOOST_TEST(oldName == c->oldName);
				BOOST_TEST(newName == c->newName);
				if (address != c->address)
					BOOST_FAIL("address");
				if (nextRollingCode != c->nextRollingCode)
					BOOST_FAIL("nextRollingCode");
				if (key != c->key)
					BOOST_FAIL("key");
				if (priority != c->priority)
					BOOST_FAIL("priority");
			}
		}

		virtual void onRemove(std::string && name) override
		{
			ExpectRemove * c = getNext<ExpectRemove>();
			if (c)
			{
				BOOST_TEST(name == c->name);
			}
		}

		virtual void onBroadcast(std::string && name, rts::SomfyFrame::Action action) override
		{
			ExpectBroadcast * c = getNext<ExpectBroadcast>();
			if (c)
			{
				BOOST_TEST(name == c->name);
				if (action != c->action)
					BOOST_FAIL("onBroadcast: action mismatch");
			}
		}

		virtual void onHelp() override
		{
			getNext<ExpectHelp>();
		}

		using ExpectedVector = std::vector<boost::variant<
			ExpectList, ExpectCreate, ExpectSet, ExpectRemove, ExpectBroadcast, ExpectHelp>>;

		std::string m_u8Input;
		bool m_ignoreCallbacks = false;
		ExpectedVector m_expected;
		ExpectedVector::iterator m_current;
	};

	CommandTester expectCommands(const char * u8Text)
	{
		return CommandTester(u8Text);
	}
}

BOOST_AUTO_TEST_CASE(CommandParser_noCommand)
{
	expectCommands(";").run();
}

BOOST_AUTO_TEST_CASE(CommandParser_list)
{
	expectCommands("list").list().run();
}

BOOST_AUTO_TEST_CASE(CommandParser_listSemiColon)
{
	expectCommands("list;").list().run();
}

BOOST_AUTO_TEST_CASE(CommandParser_create)
{
	expectCommands("create foo 1").create("foo", 1, 0).run();
}

BOOST_AUTO_TEST_CASE(CommandParser_createWithRollingCode)
{
	expectCommands("create foo 2 123").create("foo", 2, 123).run();
}

BOOST_AUTO_TEST_CASE(CommandParser_setName)
{
	expectCommands("set oldName name newName").set("oldName", "newName", {}, {}, {}, {}).run();
}

BOOST_AUTO_TEST_CASE(CommandParser_setAddress)
{
	expectCommands("set oldName address 123").set("oldName", "", 123, {}, {}, {}).run();
}

BOOST_AUTO_TEST_CASE(CommandParser_addressInvalid)
{
	BOOST_CHECK_THROW(expectCommands("set oldName address foo").runOnly(), CommandParserException);
}

BOOST_AUTO_TEST_CASE(CommandParser_rollingCode)
{
	expectCommands("set oldName rollingCode 234").set("oldName", "", {}, 234, {}, {}).run();
}

BOOST_AUTO_TEST_CASE(CommandParser_key)
{
	expectCommands("set oldName key 10").set("oldName", "", {}, {}, 10, {}).run();
}

BOOST_AUTO_TEST_CASE(CommandParser_priority)
{
	expectCommands("set oldName priority 5").set("oldName", "", {}, {}, {}, 5).run();
}

BOOST_AUTO_TEST_CASE(CommandParser_keyOutOfRange)
{
	BOOST_CHECK_THROW(expectCommands("set oldName key 256").runOnly(), CommandParserException);
}

BOOST_AUTO_TEST_CASE(CommandParser_all)
{
	expectCommands("set oldName name newName address 123 rollingCode 234 key 10 priority 6")
		.set("oldName", "newName", 123, 234, 10, 6).run();
}

BOOST_AUTO_TEST_CASE(CommandParser_remove)
{
	expectCommands("remove foo").remove("foo").run();
}

BOOST_AUTO_TEST_CASE(CommandParser_broadcast)
{
	expectCommands("broadcast foo down").broadcast("foo", rts::SomfyFrame::Action::down).run();
}

BOOST_AUTO_TEST_CASE(CommandParser_help)
{
	expectCommands("help").help().run();
}

BOOST_AUTO_TEST_CASE(CommandParser_multipleCommands)
{
	expectCommands("list;help;create \"test remote\" 123;remove \"test remote\"")
		.list()
		.help()
		.create("test remote", 123, 0)
		.remove("test remote")
		.run();
}
