/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include <iostream>

#include "TestUtils.h"
#include "remoteStore/csv/CSVRemoteStore.h"

BOOST_AUTO_TEST_CASE(TestCSVRemoteStore_write)
{
	const boost::filesystem::path tempFile = createTempFilePath();
	CSVRemoteStore store(tempFile);

	store.addRemote(RTSRemoteState(0x12345678, "name", 1, 0xab, 0xf0f0));

	std::cout << "file: " << tempFile.native() << std::endl;
}

BOOST_AUTO_TEST_CASE(TestCSVRemoteStore_addNonUniqueAddress)
{
	const boost::filesystem::path tempFile = createTempFilePath();
	CSVRemoteStore store(tempFile);

	store.addRemote(RTSRemoteState(1, "foo", 0, 0));
	BOOST_CHECK_THROW(store.addRemote(RTSRemoteState(1, "bar", 0, 0)), std::runtime_error);
}
