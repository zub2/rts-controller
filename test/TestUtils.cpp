/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TestUtils.h"

#include <boost/format.hpp>
#include <random>

namespace
{
	std::random_device randomDevice;
	std::mt19937 rdGenerator(randomDevice());
}

boost::filesystem::path createTempFilePath()
{
	boost::filesystem::path d = boost::filesystem::temp_directory_path();
	boost::filesystem::path f;

	std::uniform_int_distribution<> distribution(0, 9999);
	boost::format tempFileNameTemplate("test_temp-%04d");

	while (f.empty() || boost::filesystem::exists(f))
		f = d / (tempFileNameTemplate % distribution(rdGenerator)).str();

	return f;
}
