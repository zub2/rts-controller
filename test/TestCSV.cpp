/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>

#include <sstream>
#include <iostream>
#include <optional>

#include "remoteStore/csv/CSV.h"

struct Foo
{
	Foo(uint32_t a, uint16_t b, std::string c):
		a(a), b(b), c(std::move(c))
	{}

	Foo():
		Foo(0, 0, std::string())
	{}

	bool operator==(const Foo & other) const
	{
		return a == other.a && b == other.b && c == other.c;
	}

	uint32_t a;
	uint16_t b;
	std::string c;
};

std::ostream & operator<<(std::ostream & os, const Foo & foo)
{
	os << "{a=" << foo.a << ", b=" << foo.b << ",c='" << foo.c << "'}";
	return os;
}

struct FooHandler
{
	static void write(const Foo & foo, CSVFieldWriter & fieldWriter)
	{
		fieldWriter.write(foo.a);
		fieldWriter.write(foo.b);
		fieldWriter.write(foo.c);
	}

	static Foo read(CSVFieldReader & fieldReader)
	{
		Foo foo;
		fieldReader.read(foo.a);
		fieldReader.read(foo.b);
		fieldReader.read(foo.c);
		return foo;
	}
};

BOOST_AUTO_TEST_CASE(TestCSVRead)
{
	std::stringstream s;
	s << "11;21;row 1\n"
		<< "12;22;row 2\n"
		<< "0xaa;0xbbbb;\"quoted\nmultiline string with \\\"embedded quotes\\\"\"\n";

	CSVReader<Foo, FooHandler> csvReader(s);

	std::vector<Foo> foo;
	foo.reserve(3);

	std::optional<Foo> f;
	csvReader >> f;
	while (f)
	{
		foo.push_back(*f);
		csvReader >> f;
	}

	BOOST_TEST(foo.size() == 3);
	BOOST_TEST(foo[0] == Foo(11, 21, "row 1"));
	BOOST_TEST(foo[1] == Foo(12, 22, "row 2"));
	BOOST_TEST(foo[2] == Foo(0xaa, 0xbbbb, "quoted\nmultiline string with \"embedded quotes\""));
}

BOOST_AUTO_TEST_CASE(TestCSVWrite)
{
	std::stringstream s;
	{
		CSVWriter<Foo, FooHandler> csvWriter(s);

		csvWriter
			<< Foo(11, 21, "row 1")
			<< Foo(12, 22, "row 2")
			<< Foo(13, 23, "row 3")
			<< Foo(14, 24, "test \"escapes\" at least a bit");
	}

	const char * expected =
		"0xb;0x15;\"row 1\"\n"
		"0xc;0x16;\"row 2\"\n"
		"0xd;0x17;\"row 3\"\n"
		"0xe;0x18;\"test \\\"escapes\\\" at least a bit\"\n";

	BOOST_TEST(s.str() == expected);
}

BOOST_AUTO_TEST_CASE(TestCSVRead_noNewlineAtEnd)
{
	std::stringstream s;
	s << "11;21;row 1\n"
		<< "12;22;row 2";

	CSVReader<Foo, FooHandler> csvReader(s);

	std::vector<Foo> foo;
	foo.reserve(2);

	std::optional<Foo> f;
	csvReader >> f;
	while (f)
	{
		foo.push_back(*f);
		csvReader >> f;
	}

	BOOST_TEST(foo.size() == 2);
	BOOST_TEST(foo[0] == Foo(11, 21, "row 1"));
	BOOST_TEST(foo[1] == Foo(12, 22, "row 2"));
}
