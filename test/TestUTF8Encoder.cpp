/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>

#include <sstream>

#include "utils/UTF8Encoder.h"

using namespace std::string_literals;

template <typename T, std::size_t N>
constexpr std::size_t arraySize(T const (&)[N]) noexcept
{
	return N;
}

BOOST_AUTO_TEST_CASE(TestUTF8EncodeASCII)
{
	std::stringstream utf8Stream;
	utf8Encode(U"test"s, utf8Stream);
	BOOST_TEST(utf8Stream.str() == "test");
}

BOOST_AUTO_TEST_CASE(TestUTF8EncodeUTF8_2B)
{
	std::stringstream utf8Stream;
	utf8Encode(U"áéíóú"s, utf8Stream);
	BOOST_TEST(utf8Stream.str() == u8"áéíóú");
}

BOOST_AUTO_TEST_CASE(TestUTF8EncodeUTF8_3B)
{
	std::stringstream utf8Stream;
	utf8Encode(U"\u0800\u0801\u0802\u0803\u0804"s, utf8Stream);
	BOOST_TEST(utf8Stream.str() == u8"\u0800\u0801\u0802\u0803\u0804");
}

BOOST_AUTO_TEST_CASE(TestUTF8EncodeUTF8_4B)
{
	std::stringstream utf8Stream;
	utf8Encode(U"\U00010000\U00010001\U00010002\U00010003\U00010004"s, utf8Stream);
	BOOST_TEST(utf8Stream.str() == u8"\U00010000\U00010001\U00010002\U00010003\U00010004");
}
