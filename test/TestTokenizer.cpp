/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>

#include <functional>
#include <string>

#include <boost/variant.hpp>

#include "frontend/tcp/Tokenizer.h"

namespace
{
	class TokenTester: private TokenListener
	{
	private:
		struct ExpectWord
		{
			std::u32string s;
		};

		struct ExpectSemicolon
		{
		};

	public:
		TokenTester(std::string && u8Input):
			m_u8Input(std::move(u8Input))
		{
		}

		TokenTester & word(std::u32string && word)
		{
			m_expected.emplace_back(ExpectWord{std::move(word)});
			return *this;
		}

		TokenTester & semicolon()
		{
			m_expected.emplace_back(ExpectSemicolon{});
			return *this;
		}

		void run()
		{
			m_ignoreCallbacks = false;
			m_current = m_expected.begin();
			runImpl();

			if (m_current != m_expected.end())
				BOOST_FAIL("not all expected tokens were received");
		}

		void runOnly()
		{
			m_ignoreCallbacks = true;
			runImpl();
		}

	private:
		void runImpl()
		{
			Tokenizer t(*this);
			t.accept(m_u8Input.begin(), m_u8Input.end());
			t.end();
		}

		virtual void onWord(std::u32string && word) override
		{
			if (m_ignoreCallbacks)
				return;

			if (m_current != m_expected.end())
			{
				ExpectWord * e = boost::get<ExpectWord>(&*m_current++);
				if (e)
				{
					const bool ok = e->s == word;
					BOOST_TEST(ok); // can't just print ustring to cout, which is what BOOST_TEST tries
				}
				else
					BOOST_FAIL("received unexpected word");
			}
			else
				BOOST_FAIL("received more than expected number of tokens");
		}

		virtual void onSemicolon() override
		{
			if (m_ignoreCallbacks)
				return;

			if (m_current != m_expected.end())
			{
				ExpectSemicolon * e = boost::get<ExpectSemicolon>(&*m_current++);
				if (!e)
					BOOST_FAIL("received unexpected semicolon");
			}
			else
				BOOST_FAIL("received more than expected number of tokens");
		}

		using ExpectedVector = std::vector<boost::variant<ExpectWord, ExpectSemicolon>>;

		std::string m_u8Input;
		bool m_ignoreCallbacks = false;
		ExpectedVector m_expected;
		ExpectedVector::iterator m_current;
	};

	TokenTester expectTokens(const char * u8Text)
	{
		return TokenTester(u8Text);
	}
}

BOOST_AUTO_TEST_CASE(TestTokenizer_Word)
{
	expectTokens("test")
		.word(U"test")
		.run();
}

BOOST_AUTO_TEST_CASE(TestTokenizer_QuotedWord)
{
	expectTokens("\"test\\\"1\\\"\"")
		.word(U"test\"1\"")
		.run();
}

BOOST_AUTO_TEST_CASE(TestTokenizer_QuotedWordNoWS)
{
	BOOST_CHECK_THROW(expectTokens("\"a\"b").runOnly(), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(TestTokenizer_Command)
{
	expectTokens("foo \"bar\";")
		.word(U"foo")
		.word(U"bar")
		.semicolon()
		.run();
}

BOOST_AUTO_TEST_CASE(TestTokenizer_Commands)
{
	expectTokens("foo \"bar\";foo2 bar2; foo3  bar3 ; ")
		.word(U"foo")
		.word(U"bar")
		.semicolon()
		.word(U"foo2")
		.word(U"bar2")
		.semicolon()
		.word(U"foo3")
		.word(U"bar3")
		.semicolon()
		.run();
}

BOOST_AUTO_TEST_CASE(TestTokenizer_UnterminatedQuote)
{
	BOOST_CHECK_THROW(expectTokens("\"bar").runOnly(), std::runtime_error);
}
