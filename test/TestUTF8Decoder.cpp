/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>

#include <optional>

#include "utils/UTF8Decoder.h"

template <typename T, std::size_t N>
constexpr std::size_t arraySize(T const (&)[N]) noexcept
{
	return N;
}

BOOST_AUTO_TEST_CASE(TestUTF8DecodeASCII)
{
	UTF8Decoder decoder;

	const char utf8Str[] = u8"test";

	for (size_t i = 0; i < arraySize(utf8Str); i++)
	{
		const std::optional<char32_t> u = decoder << utf8Str[i];

		// all the elements are < 128, so the decoder should just return them
		BOOST_TEST(u.has_value());
		BOOST_TEST(*u == utf8Str[i]);
	}

	// check that there is no partially-read code point
	BOOST_TEST(decoder.codepointCompleted());
}

BOOST_AUTO_TEST_CASE(TestUTF8DecodeUTF8_2B)
{
	UTF8Decoder decoder;

	const char utf8Str[] = u8"áéíóú";
	const char32_t codepoints[] = {
		U'á', U'é', U'í', U'ó', U'ú'
	};
	static_assert(arraySize(utf8Str) - 1 == 2*arraySize(codepoints));

	for (size_t i = 0; i < arraySize(utf8Str) - 1 /*exclude terminating 0*/; i++)
	{
		const std::optional<char32_t> u = decoder << utf8Str[i];

		// every second byte a codepoint should be produced
		const bool codepointCompleted = (i % 2) == 1;
		BOOST_TEST(u.has_value() == codepointCompleted);
		BOOST_TEST(decoder.codepointCompleted() == codepointCompleted);
		if (codepointCompleted)
		{
			BOOST_TEST(*u == codepoints[i/2]);
		}
	}

	// check that there is no partially-read code point
	BOOST_TEST(decoder.codepointCompleted());
}

BOOST_AUTO_TEST_CASE(TestUTF8DecodeUTF8_3B)
{
	UTF8Decoder decoder;

	const char utf8Str[] = u8"\u0800\u0801\u0802\u0803\u0804";
	const char32_t codepoints[] = {
		U'\u0800', U'\u0801', U'\u0802', U'\u0803', U'\u0804'
	};
	static_assert(arraySize(utf8Str) - 1 == 3*arraySize(codepoints));

	for (size_t i = 0; i < arraySize(utf8Str) - 1 /*exclude terminating 0*/; i++)
	{
		const std::optional<char32_t> u = decoder << utf8Str[i];

		// every third byte a codepoint should be produced
		const bool codepointCompleted = (i % 3) == 2;
		BOOST_TEST(u.has_value() == codepointCompleted);
		BOOST_TEST(decoder.codepointCompleted() == codepointCompleted);
		if (codepointCompleted)
		{
			BOOST_TEST(*u == codepoints[i/3]);
		}
	}

	// check that there is no partially-read code point
	BOOST_TEST(decoder.codepointCompleted());
}

BOOST_AUTO_TEST_CASE(TestUTF8DecodeUTF8_4B)
{
	UTF8Decoder decoder;

	const char utf8Str[] = u8"\U00010000\U00010001\U00010002\U00010003\U00010004";
	const char32_t codepoints[] = {
		U'\U00010000', U'\U00010001', U'\U00010002', U'\U00010003', U'\U00010004'
	};
	static_assert(arraySize(utf8Str) - 1 == 4*arraySize(codepoints));

	for (size_t i = 0; i < arraySize(utf8Str) - 1 /*exclude terminating 0*/; i++)
	{
		const std::optional<char32_t> u = decoder << utf8Str[i];

		// every fourth byte a codepoint should be produced
		const bool codepointCompleted = (i % 4) == 3;
		BOOST_TEST(u.has_value() == codepointCompleted);
		BOOST_TEST(decoder.codepointCompleted() == codepointCompleted);
		if (codepointCompleted)
		{
			BOOST_TEST(*u == codepoints[i/4]);
		}
	}

	// check that there is no partially-read code point
	BOOST_TEST(decoder.codepointCompleted());
}

BOOST_AUTO_TEST_CASE(TestUTF8DecodeUTF8_invalid1)
{
	UTF8Decoder decoder;

	// unexpected continuation character
	BOOST_CHECK_THROW(decoder << 0b10111111, std::runtime_error);
}

BOOST_AUTO_TEST_CASE(TestUTF8DecodeUTF8_invalid2)
{
	UTF8Decoder decoder;

	// unexpected truncated sequence
	decoder << 0b11101111;
	decoder << 0b10111111;
	BOOST_CHECK_THROW(decoder << 0, std::runtime_error);
}

BOOST_AUTO_TEST_CASE(TestUTF8DecodeUTF8_partial)
{
	UTF8Decoder decoder;

	// unexpected truncated sequence
	decoder << 0b11101111;
	decoder << 0b10111111;
	BOOST_TEST(!decoder.codepointCompleted());
}
