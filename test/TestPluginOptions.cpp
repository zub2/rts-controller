/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>

#include <optional>
#include <string>

#include <boost/container/flat_map.hpp>

#include "PluginOptions.h"

struct TestStruct
{
	BOOST_HANA_DEFINE_STRUCT(TestStruct,
		(unsigned, number),
		(std::string, str),
		(std::optional<int>, optionalInt)
	);
};

RTS_CONTROLLER_PLUGIN_HELP(
	TestStruct,
	(number, "Help for number"),
	(str, "Help for str"),
	(optionalInt, "Help for optionalInt")
);

BOOST_AUTO_TEST_CASE(TestParseOptions_withoutOptional)
{
	const TestStruct r = parseOptions<TestStruct>({{"number", "1"}, {"str", "value"}});
	BOOST_TEST(r.number == 1);
	BOOST_TEST(r.str == "value");
	BOOST_TEST(!r.optionalInt.has_value());
}

BOOST_AUTO_TEST_CASE(TestParseOptions_withOptional)
{
	const TestStruct r = parseOptions<TestStruct>({{"number", "1"}, {"str", "value"}, {"optionalInt", "4"}});
	BOOST_TEST(r.number == 1);
	BOOST_TEST(r.str == "value");
	BOOST_TEST(r.optionalInt.has_value());
	BOOST_TEST(*r.optionalInt == 4);
}

BOOST_AUTO_TEST_CASE(TestParseOptions_withExtraField)
{
	BOOST_CHECK_THROW(parseOptions<TestStruct>({{"number", "1"}, {"str", "value"}, {"extra1", "blah"}, {"extra2", "bleh"}}), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(TestParseOptions_missingRequiredField)
{
	BOOST_CHECK_THROW(parseOptions<TestStruct>({{"number", "1"}}), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(TestParseOptions_help)
{
	std::vector<std::pair<std::string, std::string>> descriptions;
	getOptionsDescription<TestStruct>([&descriptions](const std::string & name, const std::string & help) {
		descriptions.emplace_back(name, help);
	});

	BOOST_TEST(descriptions.size() == 3);

	BOOST_TEST(descriptions[0].first == "number");
	BOOST_TEST(descriptions[0].second == "Help for number");

	BOOST_TEST(descriptions[1].first == "str");
	BOOST_TEST(descriptions[1].second == "Help for str");

	BOOST_TEST(descriptions[2].first == "optionalInt");
	BOOST_TEST(descriptions[2].second == "Help for optionalInt");
}
