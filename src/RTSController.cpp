/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RTSController.h"
#include "RTSRemote.h"
#include <stdexcept>
#include <iostream>

RTSController::RTSController(std::unique_ptr<IRemoteStore> remoteStore, std::unique_ptr<IBackend> backend):
	m_remoteStore(std::move(remoteStore)),
	m_backend(std::move(backend))
{
	boost::container::flat_map<uint32_t, RTSRemoteState> remotes = m_remoteStore->getAllRemotes();
	for (const std::pair<uint32_t, RTSRemoteState> & item : remotes)
		createRemote(item.second);
}

boost::container::flat_map<uint32_t, std::shared_ptr<IRTSRemote>> RTSController::getAllRemotes()
{
	std::scoped_lock lock(m_mutex);

	return m_remotes;
}

std::shared_ptr<IRTSRemote> RTSController::getRemote(uint32_t address)
{
	std::scoped_lock lock(m_mutex);

	const auto it = m_remotes.find(address);
	return it != m_remotes.end() ? it->second : nullptr;
}

std::shared_ptr<IRTSRemote> RTSController::getRemote(const std::string & name)
{
	std::scoped_lock lock(m_mutex);

	const TNameIndex & nameIndex = m_remoteStates.get<Name>();
	TNameIndex::const_iterator it = nameIndex.find(name);
	if (it == nameIndex.end())
		return nullptr;

	const auto itRemotes = m_remotes.find(it->address);
	return itRemotes != m_remotes.end() ? itRemotes->second : nullptr;
}

std::shared_ptr<IRTSRemote> RTSController::createRemote(uint32_t address, const std::string & name)
{
	const RTSRemoteState state(address, name, 0, 0);
	std::shared_ptr<IRTSRemote> remote = createRemote(state);
	m_remoteStore->addRemote(state);

	return remote;
}

void RTSController::send(uint32_t address, rts::SomfyFrame::Action ctrl)
{
	std::scoped_lock lock(m_mutex);

	TAddressIndex::iterator it = getRemoteState(address);
	RTSRemoteState state = *it;

	const rts::SomfyFrame frame(state.key, ctrl, state.rollingCode++, state.address);
	m_remoteStates.get<Address>().replace(it, state);
	m_remoteStore->updateRemote(state);

	m_backend->send(frame, getRepeatCountForAction(ctrl));
}

std::string RTSController::getName(uint32_t address) const
{
	std::scoped_lock lock(m_mutex);

	return getRemoteState(address)->name;
}

void RTSController::setName(uint32_t address, const std::string & name)
{
	modifyRemoteState(address, [&name](RTSRemoteState & state)
	{
		state.name = name;
	});
}

void RTSController::setAddress(uint32_t oldAddress, uint32_t newAddress)
{
	modifyRemoteState(oldAddress, [newAddress](RTSRemoteState & state)
	{
		state.address = newAddress;
	});
}

int RTSController::getPriority(uint32_t address) const
{
	std::scoped_lock lock(m_mutex);

	return getRemoteState(address)->priority;
}

void RTSController::setPriority(uint32_t address, int priority)
{
	modifyRemoteState(address, [priority](RTSRemoteState & state)
	{
		state.priority = priority;
	});
}

uint8_t RTSController::getKey(uint32_t address) const
{
	std::scoped_lock lock(m_mutex);

	return getRemoteState(address)->key;
}

void RTSController::setKey(uint32_t address, uint8_t key)
{
	modifyRemoteState(address, [key](RTSRemoteState & state)
	{
		state.key = key;
	});
}

uint16_t RTSController::getRollingCode(uint32_t address) const
{
	std::scoped_lock lock(m_mutex);

	return getRemoteState(address)->rollingCode;
}

void RTSController::setRollingCode(uint32_t address, uint16_t rollingCode)
{
	modifyRemoteState(address, [rollingCode](RTSRemoteState & state)
	{
		state.rollingCode = rollingCode;
	});
}

void RTSController::deleteRemote(uint32_t address)
{
	std::scoped_lock lock(m_mutex);
	TAddressIndex::iterator it = getRemoteState(address);
	m_remoteStates.get<Address>().erase(it);
	m_remotes.erase(address);
	m_remoteStore->removeRemote(address);
}

RTSController::TAddressIndex::iterator RTSController::getRemoteState(uint32_t address)
{
	TAddressIndex & addressIndex = m_remoteStates.get<Address>();
	TAddressIndex::iterator it = addressIndex.find(address);
	if (it == addressIndex.end())
		throw std::runtime_error("remote not found!");

	return it;
}

RTSController::TAddressIndex::const_iterator RTSController::getRemoteState(uint32_t address) const
{
	const TAddressIndex & addressIndex = m_remoteStates.get<Address>();
	TAddressIndex::const_iterator it = addressIndex.find(address);
	if (it == addressIndex.end())
		throw std::runtime_error("remote not found!");

	return it;
}

template<typename ModifyFunctor>
void RTSController::modifyRemoteState(uint32_t address, ModifyFunctor && f)
{
	std::scoped_lock lock(m_mutex);

	TAddressIndex::iterator it = getRemoteState(address);
	RTSRemoteState state = *it;
	const uint32_t oldAddress = state.address;
	f(state);
	if (!m_remoteStates.get<Address>().replace(it, state))
		throw RemoteAlreadyExistsException("remote with given name or address already exists");

	if (state.address != oldAddress)
	{
		// need to remove and add when address is changed
		m_remoteStore->removeRemote(oldAddress);
		m_remoteStore->addRemote(state);
	}
	else
		m_remoteStore->updateRemote(state);
}

std::shared_ptr<IRTSRemote> RTSController::createRemote(const RTSRemoteState & remoteState)
{
	std::scoped_lock lock(m_mutex);

	// create the instance already here to be more exception-safe
	// - if creation fails
	std::shared_ptr<IRTSRemote> remote = std::make_shared<RTSRemote>(static_cast<IRTSControllerPrivate&>(*this), remoteState.address);

	if (!m_remoteStates.insert(remoteState).second)
		throw std::runtime_error(std::string("a remote with ID ") + std::to_string(remoteState.address)
			+ " or the name " + remoteState.name + " has alread been added");

	m_remotes.insert(std::make_pair(remoteState.address, remote));

	return remote;
}

size_t RTSController::getRepeatCountForAction(rts::SomfyFrame::Action ctrl)
{
	// TODO
	return 2;
}
