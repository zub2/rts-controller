/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Logger.h"

#include <iostream>

Logger::Logger():
	m_logLevel(LogLevel::Debug)
{}

void Logger::setLogLevel(LogLevel l)
{
	std::scoped_lock lock(m_mutex);
	m_logLevel = l;
}

std::ostream & Logger::getLogStream(LogLevel level)
{
	return level == LogLevel::Error ? std::cerr : std::cout;
}

Logger & getLogger()
{
	static Logger logger;
	return logger;
}

const char * Logger::getPrefix(LogLevel level)
{
	switch (level)
	{
	case LogLevel::Debug:
		return "D ";
	case LogLevel::Info:
		return "I ";
	case LogLevel::Warning:
		return "W ";
	case LogLevel::Error:
		return "E ";
	default:
		return "? ";
	}
}
