/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LOGGER_H
#define LOGGER_H

#include <ostream>
#include <mutex>

enum class LogLevel
{
	Debug,
	Info,
	Warning,
	Error
};

class Logger
{
public:
	Logger();
	void setLogLevel(LogLevel l);

	std::scoped_lock<std::mutex> lock()
	{
		return std::scoped_lock(m_mutex);
	}

	LogLevel getLogLevel()
	{
		return m_logLevel;
	}

	std::ostream & getLogStream(LogLevel level);
	const char * getPrefix(LogLevel level);

private:
	std::mutex m_mutex;
	LogLevel m_logLevel;
};

Logger & getLogger();

// use strange variable names to decrease probability of naming clash
#define LOG(LOG_logLevel, LOG_message) \
do \
{ \
	Logger & LOG_logger = getLogger(); \
	auto LOG_lock = LOG_logger.lock(); \
	if (LOG_logLevel >= LOG_logger.getLogLevel()) \
	{ \
		LOG_logger.getLogStream(LOG_logLevel) << LOG_logger.getPrefix(LOG_logLevel) << LOG_message << std::endl; \
	} \
} while (false)

template<typename TFunctor>
void log(LogLevel level, TFunctor && f)
{
	Logger & logger = getLogger();
	auto lock = logger.lock();
	if (level >= logger.getLogLevel())
	{
		std::ostream & s = logger.getLogStream(level);
		s << logger.getPrefix(level);
		f(s);
		s << std::endl;
	}
}

#endif // LOGGER_H
