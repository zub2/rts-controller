/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PLUGIN_OPTIONS_H
#define PLUGIN_OPTIONS_H

#include <string>
#include <sstream>
#include <stdexcept>
#include <type_traits>
#include <optional>

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <boost/container/flat_map.hpp>
#include <boost/hana.hpp>

class PluginOptionsException: public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

struct PluginOptions
{
	/// Plugin name.
	std::string name;

	/// Parsed plugin options (key, value pairs).
	boost::container::flat_map<std::string, std::string> options;
};

/**
 * Parse a string in the format "name,key1=value1,key2=value2,...".
 *
 * @param[in] description The string to parse.
 * @return The parsed plugin options.
 */
PluginOptions parsePluginDescription(const std::string & description);

namespace detail
{
	template<typename T>
	struct is_optional: public std::false_type {};

	template<typename T>
	struct is_optional<std::optional<T>>: public std::true_type {};
}

template<typename Struct>
Struct parseOptions(boost::container::flat_map<std::string, std::string> && params)
{
	auto initialValues = boost::hana::transform(boost::hana::accessors<Struct>(), [&params](auto accessor) {
		constexpr auto Accessor = boost::hana::second(accessor);
		typedef std::remove_reference_t<std::invoke_result_t<decltype(Accessor),Struct&>> FieldType;
		const std::string name = boost::hana::to<char const*>(boost::hana::first(accessor));

		auto it = params.find(name);

		if constexpr (detail::is_optional<FieldType>::value)
		{
			// optional field
			if (it == params.end())
				return FieldType(); // unset optional

			FieldType r = FieldType(boost::lexical_cast<typename FieldType::value_type>(it->second));
			params.erase(it);
			return r;
		}
		else
		{
			// required field
			if (it != params.end())
			{
				FieldType r = boost::lexical_cast<FieldType>(it->second);
				params.erase(it);
				return r;
			}

			throw PluginOptionsException((boost::format("missing value for required option '%1%'!") % name).str());
		}
	});

	if (!params.empty())
	{
		std::stringstream s;

		s << "Extraneous options specified: ";

		bool first = true;
		for (const auto & it : params)
		{
			if (first)
				first = false;
			else
				s << ", ";
			s << it.first;
		}

		throw PluginOptionsException(s.str());
	}

	return boost::hana::unpack(initialValues, [](auto ...args) { return Struct{args...}; });
}

template<typename T>
struct PluginOptionsHelp;

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_1(Struct) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_2(Struct, f1) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f1)), BOOST_HANA_PP_DROP_FRONT f1) \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_3(Struct, f1, f2) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f1)), BOOST_HANA_PP_DROP_FRONT f1), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f2)), BOOST_HANA_PP_DROP_FRONT f2) \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_4(Struct, f1, f2, f3) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f1)), BOOST_HANA_PP_DROP_FRONT f1), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f2)), BOOST_HANA_PP_DROP_FRONT f2), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f3)), BOOST_HANA_PP_DROP_FRONT f3) \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_5(Struct, f1, f2, f3, f4) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f1)), BOOST_HANA_PP_DROP_FRONT f1), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f2)), BOOST_HANA_PP_DROP_FRONT f2), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f3)), BOOST_HANA_PP_DROP_FRONT f3), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f4)), BOOST_HANA_PP_DROP_FRONT f4) \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_6(Struct, f1, f2, f3, f4, f5) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f1)), BOOST_HANA_PP_DROP_FRONT f1), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f2)), BOOST_HANA_PP_DROP_FRONT f2), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f3)), BOOST_HANA_PP_DROP_FRONT f3), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f4)), BOOST_HANA_PP_DROP_FRONT f4), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f5)), BOOST_HANA_PP_DROP_FRONT f5) \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_7(Struct, f1, f2, f3, f4, f5, f6) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f1)), BOOST_HANA_PP_DROP_FRONT f1), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f2)), BOOST_HANA_PP_DROP_FRONT f2), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f3)), BOOST_HANA_PP_DROP_FRONT f3), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f4)), BOOST_HANA_PP_DROP_FRONT f4), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f5)), BOOST_HANA_PP_DROP_FRONT f5), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f6)), BOOST_HANA_PP_DROP_FRONT f6) \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_8(Struct, f1, f2, f3, f4, f5, f6, f7) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f1)), BOOST_HANA_PP_DROP_FRONT f1), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f2)), BOOST_HANA_PP_DROP_FRONT f2), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f3)), BOOST_HANA_PP_DROP_FRONT f3), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f4)), BOOST_HANA_PP_DROP_FRONT f4), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f5)), BOOST_HANA_PP_DROP_FRONT f5), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f6)), BOOST_HANA_PP_DROP_FRONT f6), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f7)), BOOST_HANA_PP_DROP_FRONT f7) \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_9(Struct, f1, f2, f3, f4, f5, f6, f7, f8) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f1)), BOOST_HANA_PP_DROP_FRONT f1), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f2)), BOOST_HANA_PP_DROP_FRONT f2), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f3)), BOOST_HANA_PP_DROP_FRONT f3), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f4)), BOOST_HANA_PP_DROP_FRONT f4), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f5)), BOOST_HANA_PP_DROP_FRONT f5), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f6)), BOOST_HANA_PP_DROP_FRONT f6), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f7)), BOOST_HANA_PP_DROP_FRONT f7), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f8)), BOOST_HANA_PP_DROP_FRONT f8) \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_10(Struct, f1, f2, f3, f4, f5, f6, f7, f8, f9) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f1)), BOOST_HANA_PP_DROP_FRONT f1), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f2)), BOOST_HANA_PP_DROP_FRONT f2), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f3)), BOOST_HANA_PP_DROP_FRONT f3), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f4)), BOOST_HANA_PP_DROP_FRONT f4), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f5)), BOOST_HANA_PP_DROP_FRONT f5), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f6)), BOOST_HANA_PP_DROP_FRONT f6), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f7)), BOOST_HANA_PP_DROP_FRONT f7), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f8)), BOOST_HANA_PP_DROP_FRONT f8), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f9)), BOOST_HANA_PP_DROP_FRONT f9) \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL_11(Struct, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10) \
template<> \
struct PluginOptionsHelp<Struct> \
{ \
	static constexpr auto apply() \
	{ \
		return boost::hana::make_map( \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f1)), BOOST_HANA_PP_DROP_FRONT f1), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f2)), BOOST_HANA_PP_DROP_FRONT f2), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f3)), BOOST_HANA_PP_DROP_FRONT f3), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f4)), BOOST_HANA_PP_DROP_FRONT f4), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f5)), BOOST_HANA_PP_DROP_FRONT f5), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f6)), BOOST_HANA_PP_DROP_FRONT f6), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f7)), BOOST_HANA_PP_DROP_FRONT f7), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f8)), BOOST_HANA_PP_DROP_FRONT f8), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f9)), BOOST_HANA_PP_DROP_FRONT f9), \
			boost::hana::make_pair(BOOST_HANA_STRING(BOOST_HANA_PP_STRINGIZE(BOOST_HANA_PP_FRONT f10)), BOOST_HANA_PP_DROP_FRONT f10) \
		); \
	} \
}

#define RTS_CONTROLLER_PLUGIN_HELP_IMPL(N, args...) \
	BOOST_HANA_PP_CONCAT(RTS_CONTROLLER_PLUGIN_HELP_IMPL_, N)(args)

#define RTS_CONTROLLER_PLUGIN_HELP(args...) \
	RTS_CONTROLLER_PLUGIN_HELP_IMPL(BOOST_HANA_PP_NARG(args), args)

template<typename Struct, typename Callback>
void getOptionsDescription(const Callback & cb)
{
	auto constexpr accessors = boost::hana::accessors<Struct>();
	auto constexpr descriptions = PluginOptionsHelp<Struct>::apply();
	static_assert(boost::hana::length(accessors) == boost::hana::length(descriptions),
		"The number of parameters and their descriptions must match");

	boost::hana::for_each(accessors, [&](auto accessor) {
		constexpr auto name = boost::hana::first(accessor);

		constexpr auto help = boost::hana::find(descriptions, name);
		static_assert(help != boost::hana::nothing, "Not all fields are described!");

		// if constexpr to avoid a slew of furter errors when a description is missing
		if constexpr (help != boost::hana::nothing)
			cb(boost::hana::to<char const*>(name), *help);
	});
}

#endif // PLUGIN_OPTIONS_H
