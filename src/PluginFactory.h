/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PLUGINFACTORY_H
#define PLUGINFACTORY_H

#include <vector>
#include <string>
#include <functional>
#include <memory>

#include <boost/container/flat_map.hpp>
#include <boost/preprocessor/cat.hpp>

#include "IPluginFactory.h"
#include "PluginOptions.h"
#include "PluginRegistry.h"
#include "pack.h"

namespace detail
{

template<typename TPluginFactoryInterface, typename TPlugin, typename TPluginOptions, typename = typename TPluginFactoryInterface::ArgsPack>
class PluginFactoryImpl;

template<typename TPluginFactoryInterface, typename TPlugin, typename TPluginOptions, typename... Args>
class PluginFactoryImpl<TPluginFactoryInterface, TPlugin, TPluginOptions, pack<Args...>>: public TPluginFactoryInterface
{
public:
	using TPluginType = TPlugin;
	using TPluginOptionsType = TPluginOptions;

	using CreateFunction = std::function<std::unique_ptr<TPlugin>(TPluginOptions &&, Args&&...)>;

	PluginFactoryImpl(std::string && name, CreateFunction && createFunction):
		m_name(std::move(name)),
		m_createFunction(std::move(createFunction))
	{}

	virtual std::string getName() override
	{
		return m_name;
	}

	virtual std::vector<std::pair<std::string, std::string>> getParametersDescription() override
	{
		std::vector<std::pair<std::string, std::string>> descriptions;
		getOptionsDescription<TPluginOptions>([&descriptions](const std::string & name, const std::string & help) {
			descriptions.emplace_back(name, help);
		});
		return descriptions;
	}

	virtual std::unique_ptr<typename TPluginFactoryInterface::IPluginType> create(boost::container::flat_map<std::string, std::string> && params, Args&&... args) override
	{
		return m_createFunction(parseOptions<TPluginOptions>(std::move(params)), std::forward<Args>(args)...);
	}

private:
	const std::string m_name;
	const CreateFunction m_createFunction;
};

}

template<typename TPluginFactoryInterface, typename TPlugin, typename TPluginOptions>
using PluginFactory = detail::PluginFactoryImpl<TPluginFactoryInterface, TPlugin, TPluginOptions>;

#define MAKE_PLUGIN_FACTORY_REGISTRATOR(TRegistry, Plugin, PluginOptions, name, createFn) \
	struct BOOST_PP_CAT(Plugin, PluginRegistrator) \
	{ \
		BOOST_PP_CAT(Plugin, PluginRegistrator)() \
		{ \
			TRegistry::get().registerFactory( \
				std::make_unique<PluginFactory<typename TRegistry::FactoryInterface, Plugin, PluginOptions>>(name, createFn) \
			); \
		} \
	}; \
	BOOST_PP_CAT(Plugin, PluginRegistrator) registrator

#endif // PLUGINFACTORY_H
