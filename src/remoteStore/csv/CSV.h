/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CSV_H
#define CSV_H

#include <istream>
#include <ostream>
#include <optional>
#include <stdexcept>
#include <type_traits>

#include "../../utils/FromString.h"

class CSVException: public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

class CSVReaderBase
{
protected:
	CSVReaderBase(std::istream & stream, const std::optional<char> & separator);

	void readField(std::string & value);

	template<typename T>
	std::enable_if_t<std::is_integral_v<T>> readField(T & value)
	{
		value = fromString<T>(readNonStringField());
	}

	std::string readNonStringField();

	void checkFieldStart();
	bool isSeparator(char c);
	void onFieldEnd(char c);
	void terminateRow();

	std::istream & m_stream;
	std::optional<char> m_separator;
	bool m_rowFinished = false;
	bool m_bad = false;

	friend class CSVFieldReader;
};

class CSVFieldReader
{
public:
	CSVFieldReader(CSVReaderBase & csvReader):
		m_csvReader(csvReader)
	{}

	template<typename T>
	void read(T & value)
	{
		m_csvReader.readField(value);
	}

	template<typename T>
	T read()
	{
		T value;
		m_csvReader.readField(value);
		return value;
	}

private:
	CSVReaderBase & m_csvReader;
};

template<typename T, typename Handler>
class CSVReader: private CSVReaderBase
{
public:
	CSVReader(std::istream & stream, const std::optional<char> & separator = std::nullopt):
		CSVReaderBase(stream, separator)
	{}

	CSVReader & operator >> (std::optional<T> & row)
	{
		row = readRow();
		return *this;
	}

	std::optional<T> readRow()
	{
		// test for EOF before the first field is even read to avoid calling the Handler at all
		if (!m_stream.eof())
		{
			// test if this is the end of the file (the last newline in file)
			// if yes, m_stream's eof bit gets set
			m_stream.get();
			if (!m_stream.eof())
				m_stream.unget();
		}

		if (m_stream.eof())
			return std::nullopt;

		CSVFieldReader fieldReader(*this);
		std::optional<T> row = Handler::read(fieldReader);
		terminateRow();
		return row;
	}
};

class CSVWriterBase
{
protected:
	CSVWriterBase(std::ostream & stream, char separator);

	void writeField(const std::string & value);

	template<typename T>
	std::enable_if_t<std::is_integral_v<T> && std::is_unsigned_v<T>> writeField(T value)
	{
		startField();

		// serialize unsigned values in hex
		m_stream << std::hex << "0x";

		// unsigned char (uint8_t) needs a cast to prevent it from being printed as character
		if constexpr (std::is_same_v<T, unsigned char>)
			m_stream << static_cast<unsigned>(value);
		else
			m_stream << value;

		m_stream << std::dec;
	}

	template<typename T>
	std::enable_if_t<std::is_integral_v<T> && std::is_signed_v<T>> writeField(T value)
	{
		startField();

		// serialize signed values in decimal
		// char (int8_t) needs a cast to prevent it from being printed as character
		if constexpr (std::is_same_v<T, char>)
			m_stream << static_cast<int>(value);
		else
			m_stream << value;
	}

	void startField();
	void terminateRow();

	std::ostream & m_stream;
	const char m_separator;
	bool m_firstField = true;

	friend class CSVFieldWriter;
};

class CSVFieldWriter
{
public:
	CSVFieldWriter(CSVWriterBase & csvWriter):
		m_csvWriter(csvWriter)
	{}

	template<typename T>
	void write(const T & value)
	{
		m_csvWriter.writeField(value);
	}

private:
	CSVWriterBase & m_csvWriter;
};

template<typename T, typename Handler>
class CSVWriter: private CSVWriterBase
{
public:
	CSVWriter(std::ostream & stream, char separator = ';'):
		CSVWriterBase(stream, separator)
	{}

	CSVWriter & operator << (const T & row)
	{
		writeRow(row);
		return *this;
	}

	void writeRow(const T & row)
	{
		CSVFieldWriter fieldWriter(*this);
		Handler::write(row, fieldWriter);
		terminateRow();
	}
};

#endif // CSV_H
