/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "CSV.h"

#include <boost/format.hpp>

CSVReaderBase::CSVReaderBase(std::istream & stream, const std::optional<char> & separator):
	m_stream(stream),
	m_separator(separator)
{}

void CSVReaderBase::readField(std::string & value)
{
	const char QUOTE = '"';
	const char ESCAPE = '\\';

	checkFieldStart();

	value.clear();
	char c;
	m_stream.get(c);
	if (m_stream.good() && c == QUOTE)
	{
		// a quoted string
		bool inEscape = false;
		while ((m_stream.get(c)) && (c != QUOTE || inEscape))
		{
			if (c == ESCAPE)
				inEscape = true;
			else
			{
				value.push_back(static_cast<char>(c));
				inEscape = false;
			}
		}

		// ... must be terminated by a quote
		if (c != QUOTE)
			throw CSVException("unterminated quoted string");

		// get the next character for the later call of onFieldEnd()
		m_stream.get(c);
	}
	else
	{
		if (m_stream.bad())
			throw CSVException("can't read from stream");

		// an unquoted string
		value.push_back(c);
		while ((m_stream.get(c)) && c != '\n'  && !isSeparator(c))
			value.push_back(c);
	}

	onFieldEnd(c);
}

std::string CSVReaderBase::readNonStringField()
{
	checkFieldStart();

	std::string value;

	char c;
	while ((m_stream.get(c)) && c != '\n'  && !isSeparator(c))
		value.push_back(static_cast<char>(c));

	onFieldEnd(c);

	return value;
}

void CSVReaderBase::checkFieldStart()
{
	if (m_rowFinished)
		throw CSVException("no more records in the current row!");
}

bool CSVReaderBase::isSeparator(char c)
{
	if (m_separator)
		return c == *m_separator;

	// if the separator is not yet known, use the first character that would make sense
	if (c == ',' || c == ';' || c == '\t')
	{
		m_separator = c;
		return true;
	}

	return false;
}

void CSVReaderBase::onFieldEnd(char c)
{
	if (m_stream.eof() || c == '\n')
		m_rowFinished = true;
	else if (!m_separator)
		m_separator = c;
	else if (c != *m_separator)
		throw CSVException((boost::format("wrong separator: '%1%', expected '%2%'") % c % *m_separator).str());
}

void CSVReaderBase::terminateRow()
{
	if (!m_rowFinished)
		throw CSVException("not all fields read from a row");
	m_rowFinished = false;
}

CSVWriterBase::CSVWriterBase(std::ostream & stream, char separator):
	m_stream(stream),
	m_separator(separator)
{}

void CSVWriterBase::writeField(const std::string & value)
{
	const char QUOTE = '"';
	const char ESCAPE = '\\';

	startField();
	m_stream << QUOTE;
	for (char c : value)
	{
		if (c == QUOTE)
			m_stream << ESCAPE;
		m_stream << c;
	}
	m_stream << QUOTE;
}

void CSVWriterBase::startField()
{
	if (m_firstField)
		m_firstField = false;
	else
		m_stream << m_separator;
}

void CSVWriterBase::terminateRow()
{
	m_stream << '\n';
	m_firstField = true;
}
