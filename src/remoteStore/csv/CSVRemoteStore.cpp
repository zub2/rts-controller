/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "CSVRemoteStore.h"

#include <fstream>
#include <string>
#include <limits>
#include <type_traits>
#include <cctype>
#include <algorithm>
#include <optional>

#include <boost/format.hpp>

#include "CSV.h"

#include "../RemoteStoreRegistrator.h"
#include "../../logger/Logger.h"

struct CSVRemoteStoreOptions
{
	BOOST_HANA_DEFINE_STRUCT(CSVRemoteStoreOptions,
		(boost::filesystem::path, fileName)
	);
};

RTS_CONTROLLER_PLUGIN_HELP(
	CSVRemoteStoreOptions,
	(fileName, "Name of CSV file storing remote configuration.")
);

namespace
{
	std::unique_ptr<CSVRemoteStore> create(CSVRemoteStoreOptions && options)
	{
		return std::make_unique<CSVRemoteStore>(std::move(options.fileName));
	}

	MAKE_REMOTE_STORE_FACTORY_REGISTRATOR(CSVRemoteStore, CSVRemoteStoreOptions, "csv", create);
}

struct RTSRemoteStateCSVHandler
{
	static RTSRemoteState read(CSVFieldReader & fieldReader)
	{
		RTSRemoteState value;

		fieldReader.read(value.address);
		fieldReader.read(value.name);
		fieldReader.read(value.key);
		fieldReader.read(value.rollingCode);
		fieldReader.read(value.priority);

		return value;
	}

	static void write(const RTSRemoteState & value, CSVFieldWriter & fieldWriter)
	{
		fieldWriter.write(value.address);
		fieldWriter.write(value.name);
		fieldWriter.write(value.key);
		fieldWriter.write(value.rollingCode);
		fieldWriter.write(value.priority);
	}
};

CSVRemoteStore::CSVRemoteStore(boost::filesystem::path storeFileName):
	m_storeFileName(std::move(storeFileName))
{
	if (m_storeFileName.empty())
		throw RemoteStoreException("store file name must not be empty!");

	read();
}

boost::container::flat_map<uint32_t, RTSRemoteState> CSVRemoteStore::getAllRemotes()
{
	return m_remoteStates;
}

void CSVRemoteStore::addRemote(const RTSRemoteState & remoteState)
{
	if (!m_remoteStates.insert(std::make_pair(remoteState.address, remoteState)).second)
		throw RemoteStoreException((boost::format("a remote with ID %1% has already been added") % std::to_string(remoteState.address)).str());

	write();
}

void CSVRemoteStore::removeRemote(uint32_t address)
{
	m_remoteStates.erase(address);
	write();
}

void CSVRemoteStore::updateRemote(const RTSRemoteState & remoteState)
{
	const auto it = m_remoteStates.find(remoteState.address);
	if (it == m_remoteStates.end())
		throw RemoteStoreException("asked to update an unknown remote");

	it->second = remoteState;

	write();
}

void CSVRemoteStore::read()
{
	LOG(LogLevel::Debug, "CSVRemoteStore::read: reading config from '" << m_storeFileName.native() << "'");
	std::ifstream f(m_storeFileName);
	if (!f.is_open())
	{
		// TODO: if the file can't be found - no problem, otherwise an error!
		LOG(LogLevel::Info, "CSVRemoteStore::read: config file does not exist - treating as empty");
		return;
	}

	CSVReader<RTSRemoteState, RTSRemoteStateCSVHandler> csvReader(f);

	m_remoteStates.clear();
	std::optional<RTSRemoteState> remoteState;

	csvReader >> remoteState;
	while (remoteState)
	{
		addRemote(*remoteState);
		csvReader >> remoteState;
	}
}

void CSVRemoteStore::write() const
{
	std::ofstream f(m_storeFileName);
	if (!f.is_open())
		throw RemoteStoreException("can't open output file!");

	CSVWriter<RTSRemoteState, RTSRemoteStateCSVHandler> csvWriter(f);

	for (const auto & item : m_remoteStates)
		csvWriter << item.second;
}
