/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CSV_REMOTE_STORE_H
#define CSV_REMOTE_STORE_H

#include <boost/filesystem.hpp>
#include <boost/container/flat_map.hpp>

#include "../IRemoteStore.h"
#include "../../RTSRemoteState.h"

class CSVRemoteStore: public IRemoteStore
{
public:
	CSVRemoteStore(boost::filesystem::path storeFileName);

	virtual boost::container::flat_map<uint32_t, RTSRemoteState> getAllRemotes() override;
	virtual void addRemote(const RTSRemoteState & remoteState) override;
	virtual void removeRemote(uint32_t address) override;
	virtual void updateRemote(const RTSRemoteState & remoteState) override;

private:
	void read();
	void write() const;

	const boost::filesystem::path m_storeFileName;
	boost::container::flat_map<uint32_t, RTSRemoteState> m_remoteStates;
};

#endif // CSV_REMOTE_STORE_H
