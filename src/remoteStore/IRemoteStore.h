/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IREMOTE_STORE_H
#define IREMOTE_STORE_H

#include <cstdint>
#include <stdexcept>
#include <boost/container/flat_map.hpp>

#include "../RTSRemoteState.h"

class RemoteStoreException: public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

class IRemoteStore
{
public:
	virtual boost::container::flat_map<uint32_t, RTSRemoteState> getAllRemotes() = 0;
	virtual void addRemote(const RTSRemoteState & remoteState) = 0;
	virtual void removeRemote(uint32_t address) = 0;
	virtual void updateRemote(const RTSRemoteState & remoteState) = 0;

	virtual ~IRemoteStore()
	{}
};

#endif // IREMOTE_STORE_H
