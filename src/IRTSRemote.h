/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IRTS_REMOTE_H
#define IRTS_REMOTE_H

#include <string>
#include <cstdint>

#include "rts/SomfyFrame.h"

class IRTSRemote
{
public:
	virtual void send(rts::SomfyFrame::Action ctrl) = 0;

	virtual std::string getName() const = 0;
	virtual void setName(const std::string & name) = 0;

	virtual uint32_t getAddress() const = 0;
	virtual void setAddress(uint32_t address) = 0;

	virtual int getPriority() const = 0;
	virtual void setPriority(int priority) = 0;

	virtual uint8_t getKey() const = 0;
	virtual void setKey(uint8_t key) = 0;

	virtual uint16_t getRollingCode() const = 0;
	virtual void setRollingCode(uint16_t rollingCode) = 0;

	virtual void deleteRemote() = 0;

	virtual ~IRTSRemote()
	{}
};

#endif // IRTS_REMOTE_H
