/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PLUGIN_REGISTRY_H
#define PLUGIN_REGISTRY_H

#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/format.hpp>
#include <boost/container/flat_set.hpp>
#include <boost/container/flat_map.hpp>

#include "PluginOptions.h"
#include "pack.h"

template<typename TFactory, typename = typename TFactory::ArgsPack>
struct PluginRegistryImpl
{};

template<typename TFactory, typename... Args>
struct PluginRegistryImpl<TFactory, pack<Args...>>
{
public:
	using FactoryInterface = TFactory;

	static PluginRegistryImpl & get()
	{
		static PluginRegistryImpl registry;
		return registry;
	}

	void registerFactory(std::unique_ptr<TFactory> && pluginFactory)
	{
		const std::string name = pluginFactory->getName();
		if (!m_factories.insert(std::make_pair(name, std::move(pluginFactory))).second)
			throw std::runtime_error((boost::format("Plugin factory '%1%' already registered!") % name).str());
	}

	boost::container::flat_set<std::string> getAllPlugins() const
	{
		boost::container::flat_set<std::string> names;
		for (const auto & entry : m_factories)
			names.insert(entry.first);

		return names;
	}

	std::vector<std::pair<std::string, std::string>> getPluginParameters(const std::string & pluginName) const
	{
		return getFactory(pluginName).getParametersDescription();
	}

	std::unique_ptr<typename TFactory::IPluginType> createPlugin(PluginOptions && po, Args&&... args) const
	{
		return getFactory(po.name).create(std::move(po.options), std::forward<Args>(args)...);
	}

	TFactory & getFactory(const std::string & name) const
	{
		auto it = m_factories.find(name);
		if (it == m_factories.end())
			throw std::runtime_error((boost::format("Can't find plugin factory for name '%1%'") % name).str());

		return *(it->second);
	}

private:
	PluginRegistryImpl()
	{}

	boost::container::flat_map<std::string, std::unique_ptr<TFactory>> m_factories;
};

template<typename TFactory>
using PluginRegistry = PluginRegistryImpl<TFactory>;

#endif // PLUGIN_REGISTRY_H
