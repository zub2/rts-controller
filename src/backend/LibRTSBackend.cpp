/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LibRTSBackend.h"

#include <boost/io/ios_state.hpp>

#include "../logger/Logger.h"

LibRTSBackend::LibRTSBackend(std::string && logName):
	m_logName(logName)
{}

void LibRTSBackend::send(const rts::SomfyFrame & frame, size_t repeatFrameCount)
{
	log(LogLevel::Info, [&frame, repeatFrameCount](std::ostream & os)
	{
		boost::io::ios_flags_saver ifs(os);
		os << "rts::broadcasting: address=0x" << std::hex << frame.getAddress()
			<< ", rollingCode=0x" << frame.getRollingCode()
			<< ", key=0x" << static_cast<unsigned>(frame.getKey())
			<< ", control=" << std::dec << static_cast<int>(frame.getCtrl())
			<< ", repeatFrameCount=" << repeatFrameCount;
	});
	m_frameTransmitter->send(frame, repeatFrameCount);
}

std::function<void(const std::string &)> LibRTSBackend::makeDebugLogger(bool verbose)
{
	std::function<void(const std::string &)> debugLogger;
	if (verbose)
		debugLogger = std::bind(&LibRTSBackend::librtsLog, this, std::placeholders::_1);

	return debugLogger;
}

void LibRTSBackend::librtsLog(const std::string & s)
{
	LOG(LogLevel::Info, "librts:" << m_logName << ": " << s);
}
