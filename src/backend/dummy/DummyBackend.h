/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef DUMMY_BACKEND_H
#define DUMMY_BACKEND_H

#include "../IBackend.h"

#include "rts/SomfyFrame.h"

class DummyBackend: public IBackend
{
public:
	virtual void send(const rts::SomfyFrame & frame, size_t repeatFrameCount) override;
};

#endif // DUMMY_BACKEND_H
