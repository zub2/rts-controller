/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "DummyBackend.h"

#include <iostream>
#include <iomanip>

#include <boost/hana.hpp>

#include "../BackendRegistrator.h"

struct DummyBackendOptions
{
	BOOST_HANA_DEFINE_STRUCT(DummyBackendOptions);
};

RTS_CONTROLLER_PLUGIN_HELP(DummyBackendOptions);

namespace
{
	std::unique_ptr<DummyBackend> create(DummyBackendOptions && options)
	{
		return std::make_unique<DummyBackend>();
	}

	MAKE_BACKEND_FACTORY_REGISTRATOR(DummyBackend, DummyBackendOptions, "dummy", create);
}

void DummyBackend::send(const rts::SomfyFrame & frame, size_t repeatFrameCount)
{
	std::cout << "DummyBackend::send: "
		"address=0x" << std::hex << frame.getAddress() << std::dec << ", "
		"rollingCode=" << frame.getRollingCode() << ", "
		"key=" << static_cast<unsigned>(frame.getKey()) << ", "
		"control=" << static_cast<int>(frame.getCtrl()) << std::endl;
}
