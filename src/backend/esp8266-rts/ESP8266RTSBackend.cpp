/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ESP8266RTSBackend.h"

#include <optional>

#include <boost/hana.hpp>

#include "rts/FrameTransmitterFactory.h"

#include "../BackendRegistrator.h"

struct ESP8266RTSBackendOptions
{
	BOOST_HANA_DEFINE_STRUCT(ESP8266RTSBackendOptions,
		(std::string, host),
		(std::optional<bool>, verbose)
	);
};

RTS_CONTROLLER_PLUGIN_HELP(
	ESP8266RTSBackendOptions,
	(host, "Host name or IP address of the ESP8266-RTS transmitter."),
	(verbose, "Be verbose.")
);

namespace
{
	const char * BACKEND_NAME = "esp8266-rts";

	std::unique_ptr<ESP8266RTSBackend> create(ESP8266RTSBackendOptions && options)
	{
		return std::make_unique<ESP8266RTSBackend>(std::move(options.host), options.verbose.value_or(false));
	}

	MAKE_BACKEND_FACTORY_REGISTRATOR(ESP8266RTSBackend, ESP8266RTSBackendOptions, BACKEND_NAME, create);
}

ESP8266RTSBackend::ESP8266RTSBackend(std::string && host, bool verbose):
	LibRTSBackend(BACKEND_NAME)
{
	rts::ESP8266RTSTransmitterConfiguration configuration =
	{
		std::move(host)
	};

	m_frameTransmitter = rts::FrameTransmitterFactory::create(configuration, makeDebugLogger(verbose));
	if (!m_frameTransmitter)
		throw std::runtime_error("Can't create RTS frame transmitter!");
}
