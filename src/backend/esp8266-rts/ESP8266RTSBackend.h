/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_BACKEND_H
#define ESP8266_RTS_BACKEND_H

#include <string>

#include "../LibRTSBackend.h"

class ESP8266RTSBackend: public LibRTSBackend
{
public:
	ESP8266RTSBackend(std::string && host, bool verbose);
};

#endif // ESP8266_RTS_BACKEND_H
