/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RPiGPIOBackend.h"

#include <optional>

#include <boost/hana.hpp>

#include "rts/FrameTransmitterFactory.h"

#include "../BackendRegistrator.h"

struct RPiGPIOBackendOptions
{
	BOOST_HANA_DEFINE_STRUCT(RPiGPIOBackendOptions,
		(unsigned, gpioNr),
		(std::optional<bool>, verbose)
	);
};

RTS_CONTROLLER_PLUGIN_HELP(
	RPiGPIOBackendOptions,
	(gpioNr, "GPIO number to use."),
	(verbose, "Be verbose.")
);

namespace
{
	const char * BACKEND_NAME = "rpi-gpio";

	std::unique_ptr<RPiGPIOBackend> create(RPiGPIOBackendOptions && options)
	{
		return std::make_unique<RPiGPIOBackend>(options.gpioNr, options.verbose.value_or(false));
	}

	MAKE_BACKEND_FACTORY_REGISTRATOR(RPiGPIOBackend, RPiGPIOBackendOptions, BACKEND_NAME, create);
}

RPiGPIOBackend::RPiGPIOBackend(unsigned gpioNr, bool verbose):
	LibRTSBackend(BACKEND_NAME)
{
	rts::RPiGPIOTransmitterConfiguration rpiGPIOConfiguration =
	{
		gpioNr
	};

	m_frameTransmitter = rts::FrameTransmitterFactory::create(rpiGPIOConfiguration, makeDebugLogger(verbose));
	if (!m_frameTransmitter)
		throw std::runtime_error("Can't create RTS frame transmitter!");
}
