/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PluginOptions.h"

#include <boost/format.hpp>

PluginOptions parsePluginDescription(const std::string & description)
{
	PluginOptions pluginOptions;

	const size_t length = description.length();
	const size_t idx = std::min(description.find(','), length);
	pluginOptions.name = description.substr(0, idx);

	if (pluginOptions.name.empty())
		throw PluginOptionsException("Plugin name can't be empty.");

	// ignore trailing ',' (in this case idx == length-1)
	size_t idxStart = idx + 1;
	while (idxStart < length)
	{
		const size_t idxEnd = std::min(description.find(',', idxStart), length);
		const size_t idxEquals = std::min(idxEnd, description.find('=', idxStart));

		const std::string key = description.substr(idxStart, idxEquals - idxStart);
		std::string value;
		if (idxEquals < idxEnd)
			value = description.substr(idxEquals + 1, idxEnd - (idxEquals + 1));

		if (key.empty())
			throw PluginOptionsException("Option name can't be empty.");

		if (!pluginOptions.options.insert(std::make_pair(key, std::move(value))).second)
			throw PluginOptionsException((boost::format("Option '%1%' specified more than once.") % key).str());

		// skip separator ','
		idxStart = idxEnd + 1;
	}

	return pluginOptions;
}
