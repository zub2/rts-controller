/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RTSRemote.h"
#include <stdexcept>

RTSRemote::RTSRemote(IRTSControllerPrivate & controller, uint32_t address):
	m_controller(controller),
	m_address(address)
{
}

void RTSRemote::send(rts::SomfyFrame::Action ctrl)
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	m_controller.send(m_address, ctrl);
}

std::string RTSRemote::getName() const
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	return m_controller.getName(m_address);
}

void RTSRemote::setName(const std::string & name)
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	m_controller.setName(m_address, name);
}

uint32_t RTSRemote::getAddress() const
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	return m_address;
}

void RTSRemote::setAddress(uint32_t address)
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	m_controller.setAddress(m_address, address);
	m_address = address;
}

int RTSRemote::getPriority() const
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	return m_controller.getPriority(m_address);
}

void RTSRemote::setPriority(int priority)
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	m_controller.setPriority(m_address, priority);
}

uint8_t RTSRemote::getKey() const
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	return m_controller.getKey(m_address);
}

void RTSRemote::setKey(uint8_t key)
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	m_controller.setKey(m_address, key);
}

uint16_t RTSRemote::getRollingCode() const
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	return m_controller.getRollingCode(m_address);
}

void RTSRemote::setRollingCode(uint16_t rollingCode)
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	m_controller.setRollingCode(m_address, rollingCode);
}

void RTSRemote::deleteRemote()
{
	std::scoped_lock lock(m_mutex);

	checkNotDeleted();
	m_controller.deleteRemote(m_address);
	m_deleted = true;
}

void RTSRemote::checkNotDeleted() const
{
	if (m_deleted)
		throw std::runtime_error("Remote has been deleted.");
}
