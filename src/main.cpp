/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <memory>
#include <cstdlib>

#include <boost/program_options.hpp>

#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>
#include <boost/format.hpp>
#include <signal.h>

#include "logger/Logger.h"
#include "RTSRemoteState.h"
#include "RTSController.h"
#include "frontend/IFrontend.h"
#include "frontend/FrontendRegistry.h"
#include "backend/IBackend.h"
#include "backend/BackendRegistry.h"
#include "remoteStore/IRemoteStore.h"
#include "remoteStore/RemoteStoreRegistry.h"

namespace
{

std::vector<std::unique_ptr<IFrontend>> createFrontends(std::vector<PluginOptions> && frontendsOptions,
	boost::asio::io_service & context, RTSController & controller)
{
	std::vector<std::unique_ptr<IFrontend>> frontends;
	for (PluginOptions & frontendOptions: frontendsOptions)
		frontends.push_back(FrontendRegistry::get().createPlugin(std::move(frontendOptions), context, controller));

	return frontends;
}

std::string getSignalName(int signalNumber)
{
	switch(signalNumber)
	{
	case SIGINT:
		return "SIGINT";
	case SIGTERM:
		return "SIGTERM";
	case SIGQUIT:
		return "SIGQUIT";
	default:
		return (boost::format("UNEXPECTED(%1%)") % signalNumber).str();
	}
}

void run(PluginOptions && storeOptions, PluginOptions && backendOptions,
	std::vector<PluginOptions> && frontendsOptions)
{
	RTSController controller(RemoteStoreRegistry::get().createPlugin(std::move(storeOptions)),
		BackendRegistry::get().createPlugin(std::move(backendOptions)));

	boost::asio::io_service context;

	boost::asio::signal_set signalSet(context, SIGINT, SIGTERM, SIGQUIT);
	signalSet.async_wait([&context](const boost::system::error_code & error, int signalNumber){
		if (error)
			LOG(LogLevel::Error, "Error in signal handler: " << error.message());
		else
			LOG(LogLevel::Info, "Received signal " << getSignalName(signalNumber));

		context.stop();
	});
	const std::vector<std::unique_ptr<IFrontend>> frontends = createFrontends(std::move(frontendsOptions),
		context, controller);

	for (const std::unique_ptr<IFrontend> & frontend : frontends)
		frontend->start();

	LOG(LogLevel::Debug, "starting io service");
	context.run();
	LOG(LogLevel::Debug, "io service stopped");

	for (const std::unique_ptr<IFrontend> & frontend : frontends)
		frontend->stop();
}

template<typename TPluginRegistry>
void printPluginHelp(const std::string & pluginName)
{
	const std::vector<std::pair<std::string, std::string>> pluginParameters =
		TPluginRegistry::get().getPluginParameters(pluginName);
	std::cout << "Available options for plugin '" << pluginName << "':\n";
	for (const auto & item: pluginParameters)
		std::cout << '\t' << item.first << ": " << item.second << '\n';
}

template<typename TPluginRegistry>
void listPluginsImpl(const char * pluginType)
{
	const boost::container::flat_set<std::string> allPlugins = TPluginRegistry::get().getAllPlugins();

	std::cout << "All available " << pluginType << ":\n";
	for (const std::string & plugin : allPlugins)
		std::cout << '\t' << plugin << '\n';
}

template<typename TPluginRegistry>
void listPlugins();

template<>
void listPlugins<FrontendRegistry>()
{
	listPluginsImpl<FrontendRegistry>("frontends");
}

template<>
void listPlugins<BackendRegistry>()
{
	listPluginsImpl<BackendRegistry>("backends");
}

template<>
void listPlugins<RemoteStoreRegistry>()
{
	listPluginsImpl<RemoteStoreRegistry>("stores");
}

template<typename TPluginRegistry>
bool parsePluginOptions(const std::string & pluginDescription, PluginOptions & po)
{
	if (pluginDescription == "help")
	{
		listPlugins<TPluginRegistry>();
		return false;
	}

	po = parsePluginDescription(pluginDescription);
	if (po.options.count("help") != 0)
	{
		printPluginHelp<TPluginRegistry>(po.name);
		return false;
	}

	return true;
}

template<typename TPluginRegistry>
bool parsePluginOptions(const boost::program_options::variables_map & variablesMap,
	const char * pluginName, PluginOptions & po)
{
	auto option = variablesMap.find(pluginName);
	if (option != variablesMap.end())
		return parsePluginOptions<TPluginRegistry>(option->second.as<std::string>(), po);

	// option not present
	return true;
}

}

std::istream & operator>>(std::istream &is, LogLevel & logLevel)
{
	std::string token;
	is >> token;

	if (token == "debug")
		logLevel = LogLevel::Debug;
	else if (token == "info")
		logLevel = LogLevel::Info;
	else if (token == "warning")
		logLevel = LogLevel::Warning;
	else if (token == "error")
		logLevel = LogLevel::Error;
	else
		throw std::runtime_error((boost::format("Can't convert '%1%' to LogLevel.") % token).str());

	return is;
}

int main(int argc, char * argv[])
{
	try
	{
		boost::program_options::options_description argDescription("Available options");
		argDescription.add_options()
			("logLevel,l", boost::program_options::value<LogLevel>(),
				"Set the log level. One of: debug, info, warning, error. Default is info.")
			("frontend,f", boost::program_options::value<std::vector<std::string>>()->required()->composing(),
				"Instantiate a frontend. The expected format is -f name,option1=value1,option2=value2,... "
				"Use -f help to list all available frontends, or -f name,help to list all options of "
				"a specific frontend.")
			("store,s", boost::program_options::value<std::string>()->required(),
				"Instantiate a remote store. The expected format is -s name,option1=value1,option2=value2,... "
				"Use -s help to list all available remote stores, or -s name,help to list all options of "
				"a specific remote store.")
			("backend,b", boost::program_options::value<std::string>()->required(),
				"Instantiate a backend. The expected format is -b name,option1=value1,option2=value2,... "
				"Use -b help to list all available backends, or -b name,help to list all options of "
				"a specific backend.")
			("help,h", "print this help")
		;

		boost::program_options::variables_map variablesMap;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, argDescription), variablesMap);

		// set the log level early in case any debug logging takes place e.g. in the listing of plugins
		LogLevel logLevel = LogLevel::Info;
		if (variablesMap.count("logLevel") != 0)
		{
			logLevel = variablesMap["logLevel"].as<LogLevel>();
		}
		getLogger().setLogLevel(logLevel);

		std::vector<PluginOptions> frontends;
		PluginOptions backend;
		PluginOptions remoteStore;

		// process help options first, before notify() to allow requesting help w/o
		// complaints about missing required args.

		if (variablesMap.count("help"))
		{
			std::cout << argDescription << '\n';
			return EXIT_SUCCESS;
		}

		auto option = variablesMap.find("frontend");
		if (option != variablesMap.end())
		{
			// check for "-f help" first
			std::vector<std::string> frontendDescriptions = option->second.as<std::vector<std::string>>();
			if (std::find(frontendDescriptions.begin(), frontendDescriptions.end(), "help") != frontendDescriptions.end())
			{
				listPlugins<FrontendRegistry>();
				return EXIT_SUCCESS;
			}

			// only now handle "-f frontend,help"
			for (const std::string & frontendDescription : frontendDescriptions)
			{
				PluginOptions frontend;
				if (!parsePluginOptions<FrontendRegistry>(frontendDescription, frontend))
					return EXIT_SUCCESS;

				frontends.push_back(frontend);
			}
		}

		if (!parsePluginOptions<RemoteStoreRegistry>(variablesMap, "store", remoteStore))
			return EXIT_SUCCESS;

		if (!parsePluginOptions<BackendRegistry>(variablesMap, "backend", backend))
			return EXIT_SUCCESS;

		boost::program_options::notify(variablesMap);

		run(std::move(remoteStore), std::move(backend), std::move(frontends));
	}
	catch (const boost::program_options::error & e)
	{
		std::cerr << e.what() << '\n';
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
