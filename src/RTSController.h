/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_CONTROLLER_H
#define RTS_CONTROLLER_H

#include <memory>
#include <mutex>
#include <stdexcept>

#include <boost/container/flat_map.hpp>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>

#include "IRTSController.h"
#include "IRTSControllerPrivate.h"
#include "IRTSRemote.h"
#include "remoteStore/IRemoteStore.h"
#include "backend/IBackend.h"

class RemoteAlreadyExistsException: public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

class RTSController: public IRTSController, private IRTSControllerPrivate
{
public:
	RTSController(std::unique_ptr<IRemoteStore> remoteStore, std::unique_ptr<IBackend> backend);

	virtual boost::container::flat_map<uint32_t, std::shared_ptr<IRTSRemote>> getAllRemotes() override;
	virtual std::shared_ptr<IRTSRemote> getRemote(uint32_t address) override;
	virtual std::shared_ptr<IRTSRemote> getRemote(const std::string & name) override;
	virtual std::shared_ptr<IRTSRemote> createRemote(uint32_t address, const std::string & name) override;

private:
	// tag types for the multi index
	struct Address {};
	struct Name {};

	typedef boost::multi_index_container<
		RTSRemoteState,
		boost::multi_index::indexed_by<
			boost::multi_index::ordered_unique<boost::multi_index::tag<Address>,
				boost::multi_index::member<RTSRemoteState,
				decltype(RTSRemoteState::address), &RTSRemoteState::address>>,
			boost::multi_index::ordered_unique<boost::multi_index::tag<Name>,
				boost::multi_index::member<RTSRemoteState,
				decltype(RTSRemoteState::name), &RTSRemoteState::name>>
		>
	> TContainer;

	typedef typename TContainer::index<Address>::type TAddressIndex;
	typedef typename TContainer::index<Name>::type TNameIndex;

	virtual void send(uint32_t address, rts::SomfyFrame::Action ctrl) override;

	virtual std::string getName(uint32_t address) const override;
	virtual void setName(uint32_t address, const std::string & name) override;

	virtual void setAddress(uint32_t oldAddress, uint32_t newAddress) override;

	virtual int getPriority(uint32_t address) const override;
	virtual void setPriority(uint32_t address, int priority) override;

	virtual uint8_t getKey(uint32_t address) const override;
	virtual void setKey(uint32_t address, uint8_t key) override;

	virtual uint16_t getRollingCode(uint32_t address) const override;
	virtual void setRollingCode(uint32_t address, uint16_t rollingCode) override;

	virtual void deleteRemote(uint32_t address) override;

	size_t getRepeatCountForAction(rts::SomfyFrame::Action ctrl);

	TAddressIndex::iterator getRemoteState(uint32_t address);
	TAddressIndex::const_iterator getRemoteState(uint32_t address) const;

	template<typename ModifyFunctor>
	void modifyRemoteState(uint32_t address, ModifyFunctor && f);

	std::shared_ptr<IRTSRemote> createRemote(const RTSRemoteState & remoteState);

	mutable std::mutex m_mutex;
	TContainer m_remoteStates;
	boost::container::flat_map<uint32_t, std::shared_ptr<IRTSRemote>> m_remotes;
	std::unique_ptr<IRemoteStore> m_remoteStore;
	std::unique_ptr<IBackend> m_backend;
};

#endif // RTS_CONTROLLER_H
