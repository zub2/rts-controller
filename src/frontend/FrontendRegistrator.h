/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FRONTEND_REGISTRATOR_H
#define FRONTEND_REGISTRATOR_H

#include "IFrontend.h"
#include "FrontendRegistry.h"
#include "../PluginFactory.h"

#define MAKE_FRONTEND_FACTORY_REGISTRATOR(TPlugin, TPluginOptions, name, createFn) \
	MAKE_PLUGIN_FACTORY_REGISTRATOR(FrontendRegistry, TPlugin, TPluginOptions, name, createFn)

#endif // FRONTEND_REGISTRATOR_H
