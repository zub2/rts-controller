/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IFRONTEND_FACTORY_H
#define IFRONTEND_FACTORY_H

#include <boost/asio.hpp>
#include "../RTSController.h"
#include "../IPluginFactory.h"
#include "IFrontend.h"

using IFrontendFactory = IPluginFactory<IFrontend, boost::asio::io_service&, RTSController&>;

#endif // IFRONTEND_FACTORY_H
