/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <string>
#include <iterator>
#include <type_traits>
#include <stdexcept>

#include "../../utils/UTF8Decoder.h"

class TokenizerException: public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

class TokenListener
{
public:
	virtual void onWord(std::u32string && word) = 0;
	virtual void onSemicolon() = 0;

	virtual ~TokenListener() = default;
};

class Tokenizer
{
public:
	Tokenizer(TokenListener & listener);

	template<typename Iterator>
	typename std::enable_if_t<std::is_same_v<typename std::iterator_traits<Iterator>::value_type, char>>
	accept(Iterator begin, const Iterator & end)
	{
		while (begin != end)
		{
			const std::optional<char32_t> codepoint = m_decoder << *begin++;
			if (codepoint)
				accept(*codepoint);
		}
	}

	void end();

private:
	enum class State
	{
		ws,
		word,
		quoted,
		quotedEscape,
		afterQuoted
	};

	void accept(char32_t c);

	void beginToken(char32_t c);
	void terminateToken(State newState = State::ws);

	TokenListener & m_listener;
	State m_state;
	UTF8Decoder m_decoder;
	std::u32string m_token;
};

#endif // TOKENIZER_H
