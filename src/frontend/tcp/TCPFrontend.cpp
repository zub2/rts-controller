/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TCPFrontend.h"

#include <functional>
#include <algorithm>
#include <optional>
#include <system_error>
#include <cstdint>
#include <vector>
#include <type_traits>
#include <iterator>
#include <iostream>

#include <boost/container/flat_map.hpp>
#include <boost/format.hpp>
#include <boost/hana.hpp>

#include "../FrontendRegistrator.h"
#include "../../logger/Logger.h"
#include "TCPConnection.h"

using namespace std::literals;
using boost::asio::ip::tcp;

std::istream & operator>>(std::istream &is, IPProto & proto)
{
	std::string token;
	is >> token;

	if (token == "ipv4")
		proto = IPProto::ipv4;
	else if (token == "ipv6")
		proto = IPProto::ipv6;
	else if (token == "both")
		proto = IPProto::both;
	else
		throw FrontendException((boost::format("Can't convert '%1%' to IPProto.") % token).str());
	return is;
}

struct TCPFrontendOptions
{
	BOOST_HANA_DEFINE_STRUCT(TCPFrontendOptions,
		(std::optional<uint16_t>, port),
		(std::optional<IPProto>, proto)
	);
};

RTS_CONTROLLER_PLUGIN_HELP(
	TCPFrontendOptions,
	(port, "Port number to listen on. Defaults to 51966."),
	(proto, "Protocol to use: ipv4, ipv6 or both. Defaults to both.")
);

namespace
{
	constexpr IPProto DEFAULT_PROTO = IPProto::both;
	constexpr uint16_t DEFAULT_PORT = 51966;

	std::unique_ptr<TCPFrontend> create(TCPFrontendOptions && options, boost::asio::io_service & ioContext, RTSController & controller)
	{
		return std::make_unique<TCPFrontend>(ioContext, controller, options.proto.value_or(DEFAULT_PROTO), options.port.value_or(DEFAULT_PORT));
	}

	MAKE_FRONTEND_FACTORY_REGISTRATOR(TCPFrontend, TCPFrontendOptions, "tcp", create);
}

TCPFrontend::TCPFrontend(boost::asio::io_service & ioContext, RTSController & controller,
	IPProto proto, uint16_t port):
	m_ioContext(ioContext),
	m_controller(controller),
	m_proto(proto),
	m_port(port)
{
}

void TCPFrontend::start()
{
	LOG(LogLevel::Info, "TCPFrontend: listening on port " << m_port);

	// https://sourceforge.net/p/asio/mailman/message/27256351/
	m_acceptor.emplace(m_ioContext);

	m_acceptor->open(m_proto == IPProto::ipv4 ? tcp::v4() : tcp::v6());
	if (m_proto != IPProto::ipv4)
	{
		// IPv6 only?
		m_acceptor->set_option(boost::asio::ip::v6_only(m_proto == IPProto::ipv6));
	}

	m_acceptor->bind(tcp::endpoint(m_proto == IPProto::ipv4 ? tcp::v4() : tcp::v6(), m_port));
	m_acceptor->listen();

	startAccept();
}

void TCPFrontend::stop()
{
	// TODO
}

void TCPFrontend::startAccept()
{
	LOG(LogLevel::Debug, "TCPFrontend: startAccept");
	std::shared_ptr<TCPConnection> connection = TCPConnection::create(m_acceptor->get_executor(), m_controller);
	m_acceptor->async_accept(connection->socket(),
		std::bind(&TCPFrontend::handleAccept, this, connection, std::placeholders::_1));
}

void TCPFrontend::handleAccept(std::shared_ptr<TCPConnection> connection, const boost::system::error_code& error)
{
	LOG(LogLevel::Debug, "TCPFrontend: handleAccept");
	if (!error)
		connection->start();
	else
		LOG(LogLevel::Error, "TCPFrontend: handleAccept: accept failed: " << error.message());

	startAccept();
}
