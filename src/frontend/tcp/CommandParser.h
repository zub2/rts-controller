/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef COMMAND_PARSER_H
#define COMMAND_PARSER_H

#include <string>
#include <vector>
#include <memory>
#include <iterator>
#include <type_traits>
#include <stdexcept>
#include <optional>

#include <boost/container/flat_map.hpp>

#include "rts/SomfyFrame.h"

#include "Tokenizer.h"

class CommandParserException: public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

class UnknownCommandException: public CommandParserException
{
	using CommandParserException::CommandParserException;
};

class CommandListener
{
public:
	virtual void onList() = 0;
	virtual void onCreate(std::string && name, uint32_t address, uint16_t nextRollingCode) = 0;
	virtual void onSet(std::string && oldName, std::string && newName, const std::optional<uint32_t> & address,
		const std::optional<uint16_t> & nextRollingCode, const std::optional<uint8_t> & key,
		const std::optional<int> & priority) = 0;
	virtual void onRemove(std::string && name) = 0;
	virtual void onBroadcast(std::string && name, rts::SomfyFrame::Action action) = 0;
	virtual void onHelp() = 0;

	virtual ~CommandListener() = default;
};

class CommandParser: private TokenListener
{
public:
	CommandParser(CommandListener & commandListener);

	template<typename Iterator>
	typename std::enable_if_t<std::is_same_v<typename std::iterator_traits<Iterator>::value_type, char>>
	accept(const Iterator & begin, const Iterator & end)
	{
		m_tokenizer.accept(begin, end);
	}

	void end();

	struct ICommandHandler
	{
		virtual void accumulateArgument(std::u32string && word) = 0;
		virtual void terminateCommand(CommandListener & commandListener) = 0;

		virtual ~ICommandHandler() = default;
	};

private:
	virtual void onWord(std::u32string && word) override;
	virtual void onSemicolon() override;

	void beginCommand(std::u32string && word);

	CommandListener & m_commandListener;
	Tokenizer m_tokenizer;
	std::unique_ptr<ICommandHandler> m_currentHandler;
	std::vector<std::u32string> m_words;
};

#endif // COMMAND_PARSER_H
