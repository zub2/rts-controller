/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Tokenizer.h"

#include <istream>
#include <cstdint>

namespace
{
	bool isWhitespace(char32_t c)
	{
		// https://en.wikipedia.org/wiki/Whitespace_character#Unicode
		switch (c)
		{
		case U'\u0009':
		case U'\u000a':
		case U'\u000b':
		case U'\u000c':
		case U'\u000d':
		case U'\u0020':
		case U'\u0085':
		case U'\u00a0':
		case U'\u1680':
		case U'\u2000':
		case U'\u2001':
		case U'\u2002':
		case U'\u2003':
		case U'\u2004':
		case U'\u2005':
		case U'\u2006':
		case U'\u2007':
		case U'\u2008':
		case U'\u2009':
		case U'\u200a':
		case U'\u2028':
		case U'\u2029':
		case U'\u202f':
		case U'\u205f':
		case U'\u3000':
			return true;

		default:
			return false;
		}
	}
}

Tokenizer::Tokenizer(TokenListener & listener):
	m_listener(listener),
	m_state(State::ws)
{}

void Tokenizer::accept(char32_t c)
{
	switch (m_state)
	{
	case State::ws:
		if (c == ';')
			m_listener.onSemicolon();
		else if (!isWhitespace(c))
			beginToken(c);
		// else: stay in ws state
		break;

	case State::word:
		if (isWhitespace(c) || c == ';')
		{
			terminateToken();
			if (c == ';')
				m_listener.onSemicolon();
		}
		else
			m_token.push_back(c); // accumulate
		break;

	case State::quoted:
		if (c == '"')
			terminateToken(State::afterQuoted);
		else if (c == '\\')
			m_state = State::quotedEscape; // begin escape
		else
			m_token.push_back(c); // accumulate
		break;

	case State::quotedEscape:
		m_token.push_back(c);
		m_state = State::quoted;
		break;

	case State::afterQuoted:
		if (c == ';')
			m_listener.onSemicolon();
		else if (!isWhitespace(c))
			throw TokenizerException("non-whitespace after the end of quoted string!");
		m_state = State::ws;
	}
}

void Tokenizer::end()
{
	if (m_state == State::word)
		terminateToken();
	else if (m_state == State::quoted || m_state == State::quotedEscape)
		throw TokenizerException("unterminated quoted word encountered!");
}

void Tokenizer::beginToken(char32_t c)
{
	if (c == '"')
	{
		// quoted token
		m_state = State::quoted;
	}
	else
	{
		// ordinary token
		m_state = State::word;
		m_token.push_back(c);
	}
}

void Tokenizer::terminateToken(State newState)
{
	m_listener.onWord(std::move(m_token));
	m_token.clear();
	m_state = newState;
}
