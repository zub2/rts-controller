/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TCPConnection.h"

#include <iostream>
#include <algorithm>

#include <boost/lexical_cast.hpp>

#include "../../utils/JSONSerializer.h"

using boost::asio::ip::tcp;

tcp::socket & TCPConnection::socket()
{
	return m_socket;
}

void TCPConnection::start()
{
	initiateReadCommand();
}

TCPConnection::~TCPConnection()
{
	LOG(LogLevel::Debug, "TCPConnection[" << this << "] dtor");
}

void TCPConnection::initiateReadCommand()
{
	LOG(LogLevel::Debug, "TCPConnection[" << this << "] initiateReadCommand");
	m_socket.async_read_some(boost::asio::buffer(m_rxBuffer, m_rxBuffer.size()),
		std::bind(&TCPConnection::handleRead, shared_from_this(), std::placeholders::_1, std::placeholders::_2));
}

void TCPConnection::handleRead(const boost::system::error_code& e, size_t length)
{
	LOG(LogLevel::Debug, "TCPConnection[" << this << "] handleRead");
	if (!e)
	{
		try
		{
			m_commandParser.accept(m_rxBuffer.begin(), m_rxBuffer.begin() + length);
			initiateReadCommand();
		}
		catch (const InvalidUtf8Exception & e)
		{
			LOG(LogLevel::Error, "TCPConnection[" << this << "] handleRead: invalid encoding: " << e.what());
			sendReply([&e](std::ostream & os) { os << "Input is not valid UTF-8 (" << e.what() << ')'; });
		}
		catch (const TokenizerException & e)
		{
			LOG(LogLevel::Error, "TCPConnection[" << this << "] handleRead: tokenizer error: " << e.what());
			sendReply([&e](std::ostream & os) { os << "Invalid input: " << e.what(); });
		}
		catch (const UnknownCommandException &)
		{
			LOG(LogLevel::Error, "TCPConnection[" << this << "] handleRead: unknown command");
			sendReply([](std::ostream & os) { os << "Unknown command. Use 'help;' to get a list of available commands."; });
		}
		catch (const CommandParserException & e)
		{
			LOG(LogLevel::Error, "TCPConnection[" << this << "] handleRead: command parser error: " << e.what());
			sendReply([&e](std::ostream & os) { os << "Command error: " << e.what(); });
		}
		catch (const std::exception &e)
		{
			LOG(LogLevel::Error, "TCPConnection[" << this << "] handleRead: parser threw exception: " << e.what());
			sendReply([](std::ostream & os) { os << "Internal error."; });
		}
	}
	else if (e == boost::asio::error::eof)
	{
		m_commandParser.end();
		LOG(LogLevel::Debug, "TCPConnection[" << this << "] handleRead: connection closed");
	}
	else
	{
		LOG(LogLevel::Error, "TCPConnection[" << this << "] handleRead: error: " << e.message());
	}
}

void TCPConnection::handleReplySent(const boost::system::error_code& e)
{
	LOG(LogLevel::Debug, "TCPConnection[" << this << "] handleReplySent");
	if (e)
		LOG(LogLevel::Debug, "TCPConnection[" << this << "] handleReplySent: error: " << e.message());
}

void TCPConnection::onList()
{
	LOG(LogLevel::Debug, "TCPConnection[" << this << "] onList");
	sendReply([this](std::ostream & os)
	{
		auto allRemotes = m_controller.getAllRemotes().extract_sequence();
		std::sort(allRemotes.begin(), allRemotes.end(), [](const auto & a, const auto & b){
			const IRTSRemote & remoteA = *(a.second);
			const IRTSRemote & remoteB = *(b.second);

			// sort lexicographically by (priority (descending), address (ascending))
			const auto prioA = remoteA.getPriority();
			const auto prioB = remoteB.getPriority();
			if (prioA != prioB)
			{
				return prioA > prioB;
			}

			return remoteA.getAddress() < remoteB.getAddress();
		});

		os << '[';

		bool first = true;
		for (const std::pair<uint32_t, std::shared_ptr<IRTSRemote>> & item : allRemotes)
		{
			const IRTSRemote & remote = *(item.second);

			if (first)
				first = false;
			else
				os << ',';

			os << '\n';
			JSONSerializer jsonSerializer(os, 1);
			jsonSerializer.add("name", remote.getName());
			jsonSerializer.add("address", remote.getAddress());
			jsonSerializer.add("rollingCode", remote.getRollingCode());
			jsonSerializer.add("key", remote.getKey());
			jsonSerializer.add("priority", remote.getPriority());
			jsonSerializer.close(false);
		}
		os << "\n]";
	});
}

void TCPConnection::onCreate(std::string && name, uint32_t address, uint16_t nextRollingCode)
{
	LOG(LogLevel::Debug, "TCPConnection[" << this << "] onCreate: address=" << address << ", name='" << name << "'");
	sendReply([this, &name, address, nextRollingCode](std::ostream & os)
	{
		try
		{
			std::shared_ptr<IRTSRemote> remote = m_controller.createRemote(address, name);
			remote->setRollingCode(nextRollingCode);
		}
		catch (const std::exception & e)
		{
			os << "FAILED: " << e.what();
			return;
		}

		os << "CREATED";
	});
}

void TCPConnection::onSet(std::string && oldName, std::string && newName, const std::optional<uint32_t> & address,
		const std::optional<uint16_t> & nextRollingCode, const std::optional<uint8_t> & key,
		const std::optional<int> & priority)
{
	LOG(LogLevel::Debug, "TCPConnection[" << this << "] onSet: name='" << oldName << "'");

	sendReply([this, &oldName, &newName, &address, &nextRollingCode, &key, &priority](std::ostream & os)
	{
		std::shared_ptr<IRTSRemote> remote = m_controller.getRemote(oldName);
		if (!remote)
		{
			os << "No remote with name '" << oldName << '\'';
			return;
		}

		if (!newName.empty())
		{
			try
			{
				remote->setName(newName);
			}
			catch (const RemoteAlreadyExistsException &)
			{
				os << "Remote with the name '" << newName << "' already exists!";
				return;
			}
		}

		if (address)
		{
			try
			{
				remote->setAddress(*address);
			}
			catch (const RemoteAlreadyExistsException &)
			{
				os << "Remote with the address " << *address << " already exists!";
				return;
			}
		}

		if (nextRollingCode)
			remote->setRollingCode(*nextRollingCode);

		if (key)
			remote->setKey(*key);

		if (priority)
			remote->setPriority(*priority);

		os << "OK";
	});
}

void TCPConnection::onRemove(std::string && name)
{
	LOG(LogLevel::Debug, "TCPConnection[" << this << "] onRemove: name='" << name << "'");
	sendReply([this, &name](std::ostream & os)
	{
		const std::shared_ptr<IRTSRemote> remote = m_controller.getRemote(name);
		if (!remote)
		{
			os << "ERROR: remote not found";
		}

		remote->deleteRemote();
		os << "REMOVED";
	});
}

void TCPConnection::onBroadcast(std::string && name, rts::SomfyFrame::Action action)
{
	LOG(LogLevel::Debug, "TCPConnection[" << this << "] onBroadcast: name='" << name << "', action=" << static_cast<int>(action));
	sendReply([this, &name, action](std::ostream & os)
	{
		const std::shared_ptr<IRTSRemote> remote = m_controller.getRemote(name);
		if (!remote)
		{
			os << "ERROR: remote not found";
			return;
		}

		remote->send(action);
		os << "OK";
	});
}

void TCPConnection::onHelp()
{
	LOG(LogLevel::Debug, "TCPConnection[" << this << "] onHelp");
	sendReply([](std::ostream & os)
	{
		os << "Available commands:\n\n"
			"* list\n"
			"List all remotes.\n\n"
			"* create name address [nextRollingCode]\n"
			"Create a new remote with given name and address. Optionally the initial rolling code "
			"can be specified. When not slecified, the initial rolling code is set to 0.\n\n"
			"* set currentName propertyName1 newPropertyValue1 ...\n\n"
			"Set one or more properties for a property with name 'currentName'. "
			"Available properties are: name, address, rollingCode, key, priority\n\n"
			"* remove name\n"
			"Remove an existing remote.\n\n"
			"* broadcast name action\n"
			"Broadcast an action. Available actions: up, down, my, prog, flag, my+up, my+down, up+down, flag, sun+flag)\n\n"
			"* help\n"
			"Print this help.";
	});
}

template<typename Functor>
void TCPConnection::sendReply(Functor && f)
{
	try
	{
		std::ostream os(&m_txBuffer);
		f(os);
		os << '\n';
	}
	catch (const std::exception & e)
	{
		m_txBuffer.consume(m_txBuffer.size());
		std::ostream os(&m_txBuffer);
		os << "Internal error: " << e.what() << '\n';

		LOG(LogLevel::Error, "TCPConnection[" << this << "] sendReply: functor threw an exception: " << e.what());
	}

	boost::asio::async_write(m_socket, m_txBuffer,
		std::bind(&TCPConnection::handleReplySent, shared_from_this(), std::placeholders::_1));
}
