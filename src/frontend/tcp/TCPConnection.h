/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TCP_CONNECTION_H
#define TCP_CONNECTION_H

#include <memory>
#include <array>
#include <string>
#include <cstdint>
#include <optional>

#include <boost/asio.hpp>

#include "../../RTSController.h"
#include "../../logger/Logger.h"
#include "CommandParser.h"

class TCPConnection: public std::enable_shared_from_this<TCPConnection>, private CommandListener
{
public:
	template<typename EXECUTOR>
	static std::shared_ptr<TCPConnection> create(const EXECUTOR & executor, RTSController & controller)
	{
		return std::shared_ptr<TCPConnection>(new TCPConnection(executor, controller));
	}

	boost::asio::ip::tcp::socket & socket();
	void start();

	~TCPConnection();

private:
	template<typename EXECUTOR>
	TCPConnection(const EXECUTOR & executor, RTSController & controller):
		m_socket(executor),
		m_controller(controller),
		m_commandParser(*this)
	{
		LOG(LogLevel::Debug, "TCPConnection[" << this << "] ctor");
	}

	void initiateReadCommand();
	void handleRead(const boost::system::error_code& e, size_t length);
	void handleReplySent(const boost::system::error_code& e);
	void executeCommand(const std::string & command, std::ostream & os);

	virtual void onList() override;
	virtual void onCreate(std::string && name, uint32_t address, uint16_t nextRollingCode) override;
	virtual void onSet(std::string && oldName, std::string && newName, const std::optional<uint32_t> & address,
		const std::optional<uint16_t> & nextRollingCode, const std::optional<uint8_t> & key,
		const std::optional<int> & priority) override;
	virtual void onRemove(std::string && name) override;
	virtual void onBroadcast(std::string && name, rts::SomfyFrame::Action action) override;
	virtual void onHelp() override;

	template<typename Functor>
	void sendReply(Functor && f);

	boost::asio::ip::tcp::socket m_socket;
	RTSController & m_controller;

	static constexpr size_t RX_BUFFER_SIZE = 1024;
	std::array<char, RX_BUFFER_SIZE> m_rxBuffer;
	boost::asio::streambuf m_txBuffer;
	CommandParser m_commandParser;
};

#endif // TCP_CONNECTION_H
