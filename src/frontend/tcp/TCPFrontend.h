/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TCP_FRONTEND_H
#define TCP_FRONTEND_H

#include <cstdint>
#include <string>
#include <optional>
#include <boost/asio.hpp>

#include "../IFrontend.h"
#include "../../RTSController.h"

class TCPConnection;

enum class IPProto
{
	ipv4,
	ipv6,
	both
};

class TCPFrontend: public IFrontend
{
public:
	TCPFrontend(boost::asio::io_service & ioContext, RTSController & controller, IPProto proto, uint16_t port);

	virtual void start() override;
	virtual void stop() override;

private:
	void startAccept();
	void handleAccept(std::shared_ptr<TCPConnection> connection, const boost::system::error_code& error);

	boost::asio::io_service & m_ioContext;
	RTSController & m_controller;
	const IPProto m_proto;
	const uint16_t m_port;
	std::optional<boost::asio::ip::tcp::acceptor> m_acceptor;
};

#endif // TCP_FRONTEND_H
