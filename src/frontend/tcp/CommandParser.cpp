/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "CommandParser.h"

#include <cstdint>
#include <limits>
#include <type_traits>
#include <string>

#include "../../utils/UTF8Encoder.h"
#include "../../utils/FromString.h"

namespace
{
	std::string parseName(const std::u32string & word)
	{
		std::string name = utf8Encode(word);
		if (name.empty())
			throw CommandParserException("name can't be empty!");

		return name;
	}

	template<typename T>
	std::enable_if_t<std::is_unsigned_v<T>, T> parseUInt(const std::u32string & word)
	{
		try
		{
			return fromString<T>(utf8Encode(word));
		}
		catch (const std::out_of_range & e)
		{
			// re-wrap in CommandParserException
			throw CommandParserException(e.what());
		}
		catch (const std::invalid_argument & e)
		{
			// re-wrap in CommandParserException
			throw CommandParserException(e.what());
		}
	}

	uint32_t parseAddress(const std::u32string & word)
	{
		return parseUInt<uint32_t>(word);
	}

	uint16_t parseRollingCode(const std::u32string & word)
	{
		return parseUInt<uint16_t>(word);
	}

	uint32_t parseKey(const std::u32string & word)
	{
		return parseUInt<uint8_t>(word);
	}

	int parsePriority(const std::u32string & word)
	{
		return fromString<int>(utf8Encode(word));
	}

	class ListCommandHandler: public CommandParser::ICommandHandler
	{
	public:
		virtual void accumulateArgument(std::u32string && word) override
		{
			throw CommandParserException("List command does not accept any arguments.");
		}

		virtual void terminateCommand(CommandListener & commandListener) override
		{
			commandListener.onList();
		}
	};

	class CreateCommandHandler: public CommandParser::ICommandHandler
	{
	public:
		virtual void accumulateArgument(std::u32string && word) override
		{
			switch (m_argIndex++)
			{
			case 0:
				m_name = parseName(word);
				break;
			case 1:
				m_address = parseAddress(word);
				break;
			case 2:
				m_rollingCode = parseRollingCode(word);
				break;
			default:
				throw CommandParserException("too many arguments");
			}
		}

		virtual void terminateCommand(CommandListener & commandListener) override
		{
			if (m_argIndex < 2)
				throw CommandParserException("too few arguments");

			commandListener.onCreate(std::move(m_name), m_address, m_rollingCode);
		}

	private:
		size_t m_argIndex = 0;
		uint32_t m_address;
		uint16_t m_rollingCode = 0;
		std::string m_name;
	};

	class SetCommandHandler: public CommandParser::ICommandHandler
	{
	private:
		enum class Property
		{
			Name,
			Address,
			NextRollingCode,
			Key,
			Priority
		};

	public:
		virtual void accumulateArgument(std::u32string && word) override
		{
			if (m_argIndex == 0)
				m_name = parseName(word);
			else if (m_argIndex % 2 == 1)
			{
				// property name
				m_currentProperty = parseProperty(word);
			}
			else
			{
				// property value
				switch (m_currentProperty)
				{
				case Property::Name:
					m_newName = parseName(word);
					break;
				case Property::Address:
					m_address = parseAddress(word);
					break;
				case Property::NextRollingCode:
					m_nextRollingCode = parseRollingCode(word);
					break;
				case Property::Key:
					m_key = parseKey(word);
					break;
				case Property::Priority:
					m_priority = parsePriority(word);
					break;
				default:
					throw std::runtime_error("Internal error: unexpected property type");
				}
			}

			m_argIndex++;
		}

		virtual void terminateCommand(CommandListener & commandListener) override
		{
			if (m_argIndex < 3)
				throw CommandParserException("too few arguments");
			if (m_argIndex % 2 != 1)
				throw CommandParserException("wrong argument count - each property needs a value");

			commandListener.onSet(std::move(m_name), std::move(m_newName), m_address,
				m_nextRollingCode, m_key, m_priority);
		}

	private:
		Property parseProperty(const std::u32string & s)
		{
			if (s == U"name")
				return Property::Name;
			else if (s == U"address")
				return Property::Address;
			else if (s == U"rollingCode")
				return Property::NextRollingCode;
			else if (s == U"key")
				return Property::Key;
			else if (s == U"priority")
				return Property::Priority;

			throw CommandParserException(std::string("unknown property '") + utf8Encode(s) + "'");
		}

		size_t m_argIndex = 0;
		std::string m_name;

		Property m_currentProperty;
		std::string m_newName;
		std::optional<uint32_t> m_address;
		std::optional<uint16_t> m_nextRollingCode;
		std::optional<uint8_t> m_key;
		std::optional<int> m_priority;
	};

	class RemoveCommandHandler: public CommandParser::ICommandHandler
	{
	public:
		virtual void accumulateArgument(std::u32string && word) override
		{
			switch (m_argIndex++)
			{
			case 0:
				m_name = parseName(word);
				break;
			default:
				throw CommandParserException("too many arguments");
			}
		}

		virtual void terminateCommand(CommandListener & commandListener) override
		{
			if (m_argIndex < 1)
				throw CommandParserException("too few arguments");

			commandListener.onRemove(std::move(m_name));
		}

	private:
		size_t m_argIndex = 0;
		std::string m_name;
	};

	class BroadcastCommandHandler: public CommandParser::ICommandHandler
	{
	public:
		virtual void accumulateArgument(std::u32string && word) override
		{
			switch (m_argIndex++)
			{
			case 0:
				m_name = parseName(word);
				break;
			case 1:
				m_action = parseAction(word);
				break;
			default:
				throw CommandParserException("too many arguments");
			}
		}

		virtual void terminateCommand(CommandListener & commandListener) override
		{
			if (m_argIndex < 2)
				throw CommandParserException("too few arguments");

			commandListener.onBroadcast(std::move(m_name), m_action);
		}

	private:

		rts::SomfyFrame::Action parseAction(const std::u32string & s)
		{
			using Action = rts::SomfyFrame::Action;

			if (s == U"my")
				return Action::my;
			else if (s == U"up")
				return Action::up;
			else if (s == U"my+up")
				return Action::my_up;
			else if (s == U"down")
				return Action::down;
			else if (s == U"my+down")
				return Action::my_down;
			else if (s == U"up+down")
				return Action::up_down;
			else if (s == U"prog")
				return Action::prog;
			else if (s == U"sun+flag")
				return Action::sun_flag;
			else if (s == U"flag")
				return Action::flag;

			throw CommandParserException(std::string("unknown action '") + utf8Encode(s) + "'");
		}

		size_t m_argIndex = 0;
		std::string m_name;
		rts::SomfyFrame::Action m_action;
	};

	class HelpCommandHandler: public CommandParser::ICommandHandler
	{
	public:
		virtual void accumulateArgument(std::u32string && word) override
		{
			throw CommandParserException("List command does not accept any arguments.");
		}

		virtual void terminateCommand(CommandListener & commandListener) override
		{
			commandListener.onHelp();
		}
	};

	template<typename T>
	std::unique_ptr<CommandParser::ICommandHandler> createCommandHandler()
	{
		return std::make_unique<T>();
	}

	using HandlerCreateFunction = std::unique_ptr<CommandParser::ICommandHandler>(*)();
	const boost::container::flat_map<std::u32string, HandlerCreateFunction> COMMANDS =
	{
		{ U"list", createCommandHandler<ListCommandHandler> },
		{ U"create", createCommandHandler<CreateCommandHandler> },
		{ U"set", createCommandHandler<SetCommandHandler> },
		{ U"remove", createCommandHandler<RemoveCommandHandler> },
		{ U"broadcast", createCommandHandler<BroadcastCommandHandler> },
		{ U"help", createCommandHandler<HelpCommandHandler> }
	};
}

CommandParser::CommandParser(CommandListener & commandListener):
	m_commandListener(commandListener),
	m_tokenizer(*this)
{}

void CommandParser::end()
{
	m_tokenizer.end();

	// synthetic ; to not force the ; in simple situations (just 1 command)
	onSemicolon();
}

void CommandParser::onWord(std::u32string && word)
{
	if (!m_currentHandler)
		beginCommand(std::move(word));
	else
		m_currentHandler->accumulateArgument(std::move(word));
}

void CommandParser::onSemicolon()
{
	if (m_currentHandler)
	{
		m_currentHandler->terminateCommand(m_commandListener);
		m_currentHandler.reset();
	}
	// else: a ; without any word -> do nothing
}

void CommandParser::beginCommand(std::u32string && word)
{
	auto it = COMMANDS.find(word);
	if (it == COMMANDS.end())
		throw UnknownCommandException("unknown command");

	m_currentHandler = (it->second)();
}
