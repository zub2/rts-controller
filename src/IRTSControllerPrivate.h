/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IRTS_CONTROLLER_PRIVATE_H
#define IRTS_CONTROLLER_PRIVATE_H

#include <cstdint>
#include <string>

class IRTSControllerPrivate
{
public:
	virtual void send(uint32_t address, rts::SomfyFrame::Action ctrl) = 0;

	virtual std::string getName(uint32_t address) const = 0;
	virtual void setName(uint32_t address, const std::string & name) = 0;

	virtual void setAddress(uint32_t oldAddress, uint32_t newAddress) = 0;

	virtual int getPriority(uint32_t address) const = 0;
	virtual void setPriority(uint32_t address, int priority) = 0;

	virtual uint8_t getKey(uint32_t address) const = 0;
	virtual void setKey(uint32_t address, uint8_t key) = 0;

	virtual uint16_t getRollingCode(uint32_t address) const = 0;
	virtual void setRollingCode(uint32_t address, uint16_t rollingCode) = 0;

	virtual void deleteRemote(uint32_t address) = 0;

	virtual ~IRTSControllerPrivate()
	{}
};

#endif // IRTS_CONTROLLER_PRIVATE_H
