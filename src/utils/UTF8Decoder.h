/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef UTF8_DECODER_H
#define UTF8_DECODER_H

#include <optional>
#include <cstdint>
#include <stdexcept>

class InvalidUtf8Exception: public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

class UTF8Decoder
{
public:
	UTF8Decoder():
		m_remainingBytes(0),
		m_state(0)
	{}

	std::optional<char32_t> operator<<(char c)
	{
		const uint8_t b = static_cast<uint8_t>(c);

		if (m_remainingBytes != 0)
		{
			// the byte must be a continuation byte
			if ((b & 0b11000000) != 0b10000000)
				throw InvalidUtf8Exception("can't decode input as utf8: continuation byte expected");

			m_state = (m_state << 6) | (b & 0b00111111);
			m_remainingBytes--;
		}
		else
		{
			// the byte must start a code point
			uint32_t valueMask;
			if ((b & 0b10000000) == 0)
			{
				// a code point that fits in 1 byte
				valueMask = 0b01111111;
			}
			else if ((b & 0b11100000) == 0b11000000)
			{
				// start of a codepoint encoded in 2 bytes
				valueMask = 0b00011111;
				m_remainingBytes = 1;
			}
			else if ((b & 0b11110000) == 0b11100000)
			{
				// start of a codepoint encoded in 3 bytes
				valueMask = 0b00001111;
				m_remainingBytes = 2;
			}
			else if ((b & 0b11111000) == 0b11110000)
			{
				valueMask = 0b00000111;
				// start of a codepoint encoded in 4 bytes
				m_remainingBytes = 3;
			}
			else
				throw InvalidUtf8Exception("can't decode input as utf8: wrong start of codepoint");

			m_state = b & valueMask;
		}

		return codepointCompleted() ? std::optional<char32_t>(m_state) : std::nullopt;
	}

	bool codepointCompleted() const
	{
		return m_remainingBytes == 0;
	}

private:
	unsigned m_remainingBytes;
	uint32_t m_state;
};

#endif // UTF8_DECODER_H
