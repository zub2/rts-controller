/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "UTF8Encoder.h"

#include <sstream>

namespace
{
	void utf8Encode(char32_t c, std::ostream & dest)
	{
		uint32_t code = static_cast<uint32_t>(c);
		if (code <= 0x80)
			dest << static_cast<char>(code);
		else if (code < 0x800)
		{
			dest << static_cast<char>(0b11000000 | (code >> 6));
			dest << static_cast<char>(0b10000000 | (code & 0b111111));
		}
		else if (code < 0x10000)
		{
			dest << static_cast<char>(0b11100000 | (code >> 2*6));
			dest << static_cast<char>(0b10000000 | ((code >> 6) & 0b111111));
			dest << static_cast<char>(0b10000000 | (code & 0b111111));
		}
		else if (code < 0x110000)
		{
			dest << static_cast<char>(0b11110000 | (code >> 3*6));
			dest << static_cast<char>(0b10000000 | ((code >> 2*6) & 0b111111));
			dest << static_cast<char>(0b10000000 | ((code >> 6) & 0b111111));
			dest << static_cast<char>(0b10000000 | (code & 0b111111));
		}
		else
			throw UnicodeException("encountered character out of unicode range");
	}
}

void utf8Encode(const std::u32string & s, std::ostream & dest)
{
	for (size_t i = 0; i < s.size(); i++)
		utf8Encode(s[i], dest);
}

std::string utf8Encode(const std::u32string & s)
{
	std::stringstream utf8Stream;
	utf8Encode(s, utf8Stream);
	return utf8Stream.str();
}
