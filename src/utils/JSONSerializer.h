/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef JSON_SERIALIZER_H
#define JSON_SERIALIZER_H

#include <ostream>
#include <string>

class JSONSerializer
{
public:
	JSONSerializer(std::ostream & os, size_t indentLevel);
	void add(const std::string & key, const std::string & value);
	void add(const std::string & key, uint8_t value);

	template<typename T>
	void add(const std::string & key, const T & value)
	{
		outputKey(key);
		m_outputStream << value;
	}

	void close(bool addNewline);

	~JSONSerializer();

private:
	void outputKey(const std::string & key);
	void indent();
	void ensureOpened();
	void jsonEscape(const std::string & s);
	void jsonEscape(char c);

	std::ostream & m_outputStream;
	const size_t m_indentLevel;
	bool m_isOpened;
	bool m_firstKey;
};

#endif // JSON_SERIALIZER_H
