/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FROM_STRING
#define FROM_STRING

#include <type_traits>
#include <string>
#include <limits>
#include <stdexcept>
#include <sstream>

template<typename T>
std::enable_if_t<
	std::is_integral_v<T> || std::is_same_v<T, bool>,
	std::conditional_t<std::is_signed_v<T>, long long, unsigned long long>
> fromString(const std::string & s)
{
	constexpr bool isSigned = std::is_signed_v<T>;
	size_t pos;
	std::conditional_t<isSigned, long long, unsigned long long> n;

	auto getRangeErrorStr = [&s]{
		std::stringstream ss;
		ss << "value " << s << " can't fit in target type (" <<
			std::to_string(std::numeric_limits<T>::min()) << ".." <<
			std::to_string(std::numeric_limits<T>::max()) + ")";
		return ss.str();
	};

	try
	{
		if constexpr (isSigned)
			n = std::stoll(s, &pos, 0);
		else
			n = std::stoull(s, &pos, 0);
	}
	catch(const std::invalid_argument &)
	{
		// the error string is not too useful, so provide something better
		throw std::invalid_argument("can't convert string '" + s + "' to number");
	}
	catch(const std::out_of_range &)
	{
		// this is probably also not useful w/o more info
		throw std::out_of_range(getRangeErrorStr());
	}

	// there can be no other character (not even whitespace) after the number
	if (pos != s.length())
		throw std::invalid_argument("argument is not a number");

	if (n > std::numeric_limits<T>::max() || n < std::numeric_limits<T>::min())
		throw std::out_of_range(getRangeErrorStr());

	return static_cast<T>(n);
}

#endif // FROM_STRING
