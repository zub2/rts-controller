/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "JSONSerializer.h"

#include <iomanip>

#include <boost/io/ios_state.hpp>

JSONSerializer::JSONSerializer(std::ostream & os, size_t indentLevel):
	m_outputStream(os),
	m_indentLevel(indentLevel),
	m_isOpened(false),
	m_firstKey(true)
{}

void JSONSerializer::add(const std::string & key, uint8_t value)
{
	add(key, static_cast<uint16_t>(value));
}

void JSONSerializer::add(const std::string & key, const std::string & value)
{
	outputKey(key);

	m_outputStream << '"';
	jsonEscape(value);
	m_outputStream << '"';
}

void JSONSerializer::close(bool addNewline)
{
	if (m_isOpened)
	{
		m_outputStream << '\n';
		indent();
		m_outputStream << '}';
		if (addNewline)
			m_outputStream << '\n';
		m_isOpened = false;
	}
}

JSONSerializer::~JSONSerializer()
{
	close(false);
}

void JSONSerializer::outputKey(const std::string & key)
{
	ensureOpened();

	if (!m_firstKey)
		m_outputStream << ',';
	else
		m_firstKey = false;

	m_outputStream << '\n';
	indent();
	m_outputStream << "\t\"";
	jsonEscape(key);
	m_outputStream << "\": ";
}

void JSONSerializer::indent()
{
	for (size_t i = 0; i < m_indentLevel; i++)
		m_outputStream << '\t';
}

void JSONSerializer::ensureOpened()
{
	if (!m_isOpened)
	{
		indent();
		m_outputStream << '{';
		m_isOpened = true;
	}
}

void JSONSerializer::jsonEscape(const std::string & s)
{
	// this assumes input is in UTF-8 and no codepoint > 127 needs escaping
	for (char c : s)
	{
		if (static_cast<uint8_t>(c) < 0x80)
			jsonEscape(c);
		else
		{
			// a code point that is encoded in more than 1 byte - just pass it through
			m_outputStream << c;
		}
	}
}

void JSONSerializer::jsonEscape(char c)
{
	const uint8_t b = static_cast<uint8_t>(c);
	if (b < 0x20)
	{
		boost::io::ios_flags_saver ifs(m_outputStream);
		boost::io::ios_width_saver iws(m_outputStream);

		m_outputStream << "\\u" << std::hex << std::setfill('0') << std::setw(4)
			<< static_cast<unsigned>(b);
	}
	else if (c =='\\')
		m_outputStream << "\\\\";
	else if (c == '"')
		m_outputStream << "\\\"";
	else
		m_outputStream << c;
}
