/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_REMOTE_H
#define RTS_REMOTE_H

#include <cstdint>
#include <mutex>

#include "IRTSRemote.h"
#include "IRTSControllerPrivate.h"

class RTSRemote: public IRTSRemote
{
public:
	RTSRemote(IRTSControllerPrivate & controller, uint32_t address);

	virtual void send(rts::SomfyFrame::Action ctrl) override;

	virtual std::string getName() const override;
	virtual void setName(const std::string & name) override;

	virtual uint32_t getAddress() const override;
	virtual void setAddress(uint32_t address) override;

	virtual int getPriority() const override;
	virtual void setPriority(int priority) override;

	virtual uint8_t getKey() const override;
	virtual void setKey(uint8_t key) override;

	virtual uint16_t getRollingCode() const override;
	virtual void setRollingCode(uint16_t rollingCode) override;

	virtual void deleteRemote() override;

private:
	void checkNotDeleted() const;

	mutable std::mutex m_mutex;
	IRTSControllerPrivate & m_controller;
	uint32_t m_address;
	bool m_deleted = false;
};

#endif // RTS_REMOTE_H
