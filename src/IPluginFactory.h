/*
 * Copyright 2018 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-controller.
 *
 * rts-controller is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-controller is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-controller.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IPLUGINFACTORY_H
#define IPLUGINFACTORY_H

#include <vector>
#include <string>
#include <memory>

#include <boost/container/flat_map.hpp>

#include "pack.h"

template<typename IPlugin, typename... Args>
class IPluginFactory
{
public:
	using IPluginType = IPlugin;
	using ArgsPack = pack<Args...>;

	virtual std::string getName() = 0;
	virtual std::vector<std::pair<std::string, std::string>> getParametersDescription() = 0;
	virtual std::unique_ptr<IPlugin> create(boost::container::flat_map<std::string, std::string> && params, Args&&... args) = 0;

	virtual ~IPluginFactory()
	{}
};

#endif // IPLUGINFACTORY_H
