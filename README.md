# rts-controller
Program that can control devices using the Somfy RTS protocol. The protocol is used by some blinds, shades, rolling shutters, awnings, etc. by [Somfy](https://www.somfysystems.com/).

This program keeps track of defined remotes (e.g. its rolling counter). It uses [librts](https://gitlab.com/zub2/rts-tools/#librts) for the actual RF communication.

## Cloning
Please note that this repository uses [git submodule](https://git-scm.com/docs/git-submodule) to fetch librts. So either clone this repository with `--recurse-submodules`, or do `git submodule init && git submodule update` after checkout.

## Compiling

[meson](http://mesonbuild.com/), a C++ compiler supporting C++ 17 and the [boost](http://boost.org/) libraries (tested with version 1.62) are required. Please note that because of [gcc bug 86429](https://gcc.gnu.org/bugzilla/show_bug.cgi?id=86429) the source will not compile in affected versions of gcc. Compilation in clang seems OK (tested with clang 6.0.1).

## Hardware setup
To use this program a hardware setup supported by librts is needed. There are two reasonable options for broadcasting RTS frames:
* [a custom, but easy to make, board](https://gitlab.com/zub2/esp8266-rts) - this works very well
* a Raspberry Pi and an OOK transmitter controlled by one of the RPi's GPIO pins. I'm using the ubiquitous and cheap [MX-FS-03V](http://hobbycomponents.com/wired-wireless/168-433mhz-wireless-modules-mx-fs-03v-mx-05). It works but its range is quite limited (~5 meters for me). Please see [rts-tools](https://gitlab.com/zub2/rts-tools) for more info about hardware.

## Software setup
If you use the GPIO-controlled OOK transmitter, the GPIO line needs to be configured before you run the RTS Controller. (RTS Controller directly accesses GPIO memory to transmit data but it does not set the GPIO direction - that has to be done beforehand.)

You need to do something like the following on the Raspberry Pi (replace the `4` with the GPIO number of the pin to which you transmitter is connected):

```shell
# echo 4 > /sys/class/gpio/export
# echo out > /sys/class/gpio/gpio4
```

There's an example GPIO setup script in `examples/setup-gpio.sh`.

## Usage
You can get help on the available options by running `rts-controller -h`. In general you have to pick a backend, a store and a frontend. That is currently an easy choice as there is just 1 frontend, 1 store and 2 backends. :-)

To list available backends, run `rts-controller -b help`. Similarly for frontends (`-f`) and stores (`-s`). To list arguments supported by a backend, frontend or remote store, use `name,help`, e.g. to list arguments supported by the backend `rts` run `rts-controller -b rts,help`. To pass arguments to a backend, frontend or store, use `name,key1=value1,key2=value2,...`. E.g. to pass `port=1234` to the `tcp` frontend use `-f tcp,port=1234`.

### Backends
There are two backends:

* `dummy`: logs the data it would send and does nothing (it's useful for testing)
* `rts`: uses librts to send the actual frames; only works on Raspberry Pi (tested on version 1 and 3)

### Frontends
There is just one frontend: `tcp`. The frontend listens on given port number and interprets incoming data as commands. This is how remotes can be added and commands can be broadcast.

#### TCP Frontend Command syntax
Each command must be terminated either by `;` or by connection end. Strings that contain a whitespace or `;` need to be quoted. `\"` in a quoted string can be used to escape a quote.

Use `help;` to get a list of supported commands and some basic info about them.

### Stores
There is just one remote store, named `csv`. It stores the state of defined remotes in a csv file.

### Examples

#### Running rts-controller directly
To run the RTS Controller with:

* debug logging
* dummy backend
* listening on TCP port 1234
* state stored in `rts-state.csv`

run `rts-controller` like this:

```shell
$ rts-controller -l debug -b dummy -f tcp,port=1234 -s csv,fileName=rts-state.csv
```

Then you can connect to localhost:1234 e.g. via netcat and issue commands. For example:
```
$ nc localhost 1234
help;
Available commands:
list - list all remotes
create name address [nextRollingCode] - create a new remote
set currentName property1 value1 ... - set properties (name, address, rollingCode, key)
remove name - remove an existing remote
broadcast name action - broadcast a command
help - print this help
```

Alternatively you can use the following:
```
$ echo help | nc -N localhost 1234
Available commands:
list - list all remotes
create name address [nextRollingCode] - create a new remote
set currentName property1 value1 ... - set properties (name, address, rollingCode, key)
remove name - remove an existing remote
broadcast name action - broadcast a command
help - print this help
```

To run RTS Controller with the real backend do:
```shell
# rts-controller -l debug -b rts,gpioNr=4 -f tcp,port=1234 -s csv,fileName=rts-state.csv
```
(You will need to run this as root, as the raw GPIO access - at least they way it's implemented in librts - needs root access.)

Then add at least one remote, e.g.:
```
$ echo "create living_room 10000" | nc -N localhost 1234
CREATED
```

You can see all defined remotes and their states:
```
$ echo "list" | nc -N localhost 1234
[
	{
		"name": "living_room",
		"address": 10000,
		"rollingCode": 0,
		"key": 0
	}
]
```

To send an actual command do:
```
$ echo "broadcast living_room down" | nc -N localhost 1234
OK
```

#### Automating startup

You can use the files in `examples`: There is a shell script that configures GPIO and a systemd unit file that can be used to launch the RTS Controller.

It's possible to connect the RTS Controller to OpenHAB via OpenHAB's tcp plugin.

## TODOs

Here are some of the things I'm considering adding:

* add a protobuf frontend + CLI client
* keep track of the state of each blind; as there is no feedback, it can only be modelled and estimated, but perhaps something useful can still be done
* add a simple dedicated Android application (OpenHAB2 on the RPi 1 is quite slow)
